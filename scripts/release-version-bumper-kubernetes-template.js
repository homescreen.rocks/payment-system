// standard-version-updater for kubernetes.yml
const yaml = require('js-yaml');
const fs = require('fs');

module.exports.readVersion = function (contents) {
    try {
        const doc = yaml.loadAll(fs.readFileSync(__dirname + '/../deployment/kubernetes.yml', 'utf8'));
        const image = doc[1].spec.template.spec.containers[0].image;
        const imageVersion = image.split(':')
        return imageVersion[1];
    } catch (e) {
        console.log(e);
    }
}

module.exports.writeVersion = function (contents, version) {
    try {
        const doc = yaml.loadAll(contents);
        const image = doc[1].spec.template.spec.containers[0].image;
        const imageVersion = image.split(':')
        const imageName = imageVersion[0];
        doc[1].spec.template.spec.containers[0].image = imageName + ':' + version;
        return doc.map(singleDoc => yaml.dump(singleDoc)).join('---\n');
    } catch (e) {
        console.log(e);
    }
    return stringifyPackage(json, indent, newline)
}