#!/usr/bin/python3

import base64
import logging
import mimetypes
from getpass import getpass
from pprint import pprint

import requests
from openpyxl import load_workbook


def get_data_url_from_filename(filename):
    with open('images/' + filename, mode='rb') as file:  # b is important -> binary
        file_content = file.read()
    b64 = base64.standard_b64encode(file_content)
    mime_type = mimetypes.MimeTypes().guess_type('images/' + filename)[0]

    return 'data:{mime};base64,{b64}'.format(mime=mime_type, b64=b64.decode())


logging.basicConfig(level=logging.DEBUG)

SERVER_ADDRESS = 'https://localhost:5001'

LOGIN_URL = SERVER_ADDRESS + "/api/v1/login"
CREATE_PRODUCT_TYPE_URL = SERVER_ADDRESS + "/api/v1/producttypes"
GET_PRODUCT_TYPE_URL = SERVER_ADDRESS + "/api/v1/producttypes?filters=Name=={pt}"
CREATE_PRODUCT_URL = SERVER_ADDRESS + "/api/v1/products"
CREATE_POINT_OF_SALE_URL = SERVER_ADDRESS + '/api/v1/PointsOfSale'
GET_ROLE_URL = SERVER_ADDRESS + '/api/v1/roles?filters=Name=={role}'
CREATE_USER_ACCOUNT_URL = SERVER_ADDRESS + '/api/v1/accounts'
LOCK_USER_ACCOUNT_URL = SERVER_ADDRESS + '/api/v1/accounts/{account_id}/lock'
GET_USER_ACCOUNT_URL = SERVER_ADDRESS + '/api/v1/accounts/?filters=Name=={account_name}'

logging.info("logging in ...")

username = input('Enter Admin user: ')
password = getpass('Enter Admin password: ')

loginMessage = {
    "username": username,
    "password": password
}

resp = requests.post(LOGIN_URL, json=loginMessage)

data = resp.json()
token = data['token']
logging.debug("TOKEN is %s" % token)

headers = {
    'Authorization': 'Bearer %s' % token
}

wb = load_workbook('data.xlsx', data_only=True)
logging.debug(wb.sheetnames)

products_ws = wb['products']

ws_product_types = products_ws['D']

product_types = {}

for pt_cell in ws_product_types[1:]:

    pt = pt_cell.value

    if product_types.get(pt) is not None:
        continue

    product_types[pt] = None

    create_product_type_msg = {
        'Name': pt
    }
    resp = requests.post(CREATE_PRODUCT_TYPE_URL, json=create_product_type_msg, headers=headers)
    if resp.status_code == 400:
        logging.debug('already in DB, getting information via GET')
        resp = requests.get(GET_PRODUCT_TYPE_URL.format(pt=pt), headers=headers)

    resp_data = resp.json()
    if isinstance(resp_data, list):
        product_types[pt] = resp_data[0]['id']
    else:
        product_types[pt] = resp_data['id']

pprint(product_types)
logging.info('DONE creating product types')

logging.info('START creating products')

for row in products_ws.iter_rows(min_row=2, min_col=1):
    data = {
        'name': row[0].value,
        'description': row[1].value,
        'price': row[2].value,
        'productTypeId': product_types[row[3].value],
        'image': get_data_url_from_filename(row[4].value)
    }
    pprint(data)
    resp = requests.post(CREATE_PRODUCT_URL, json=data, headers=headers)

logging.info('DONE creating products')

logging.info('START creating PointOfSales')

pos_ws = wb['pos']

for row in pos_ws.iter_rows(min_row=2, min_col=1):
    data = {
        'name': row[0].value
    }
    resp = requests.post(CREATE_POINT_OF_SALE_URL, json=data, headers=headers)

logging.info('DONE creating PointOfSales')

# TODO create Accounts

roles = {
    'Admin': None,
    'User': None,
    'NfcClient': None,
    'CashDesk': None
}

logging.info('getting role information')

for role in roles:
    resp = requests.get(GET_ROLE_URL.format(role=role), headers=headers)
    data = resp.json()
    if len(data) != 1:
        raise Exception('More than one value in list returned')
    roles[role] = data[0]['id']

pprint(roles)

ws_accounts = wb['accounts']

for row in ws_accounts.iter_rows(min_row=2, min_col=1):
    username = row[0].value
    password = row[1].value
    card_id = row[2].value
    locked = row[3].value
    excel_role_list = row[4].value

    if excel_role_list is None:
        role_id_list = []
    else:
        role_id_list = [{'id': roles[r], 'name': r} for r in excel_role_list.split(',')]

    data = {
        'username': username,
        'password': password,
        'cardId': card_id,
        'roles': role_id_list
    }
    logging.warning('DATA START ===============')
    pprint(data)
    logging.warning('DATA END ===============')
    resp = requests.post(CREATE_USER_ACCOUNT_URL, json=data, headers=headers)
    logging.warning('resp START ===============')
    pprint(vars(resp))
    logging.warning('resp END ===============')
    if resp.status_code != 400:
        resp_data = resp.json()
        account_id = resp_data['id']
        pprint(account_id)
        if locked is True:
            lock_resp = requests.patch(LOCK_USER_ACCOUNT_URL.format(account_id=account_id), headers=headers)
            logging.warning('lock_resp START ===============')
            pprint(vars(lock_resp))
            logging.warning('lock_resp END ===============')

# TODO ask for username/password of admin user and delete old admin

admin_user = input('Admin Username: ')

while True:
    admin_password = getpass('Admin Password: ')
    admin_password2 = getpass('Type it again: ')

    if admin_password == admin_password2:
        break
    else:
        logging.error('Passwords didn\'t match, try again...')

create_data = {
    'username': admin_user,
    'password': admin_password,
    'cardId': None,
    'roles': [{
        'id': roles['Admin'],
        'name': 'Admin'
    }]
}
resp = requests.post(CREATE_USER_ACCOUNT_URL, json=create_data, headers=headers)

# find old admin
resp_old_admin = requests.get(GET_USER_ACCOUNT_URL.format(account_name='admin'), headers=headers)
old_admin_account_id = resp_old_admin.json()[0]['id']

logging.warning('Please delete the user with the following id from the database: ' + old_admin_account_id)
