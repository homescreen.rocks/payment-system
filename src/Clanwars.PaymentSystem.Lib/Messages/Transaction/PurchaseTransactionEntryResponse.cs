using System;
using Clanwars.PaymentSystem.Lib.Messages.Product;

namespace Clanwars.PaymentSystem.Lib.Messages.Transaction;

public record PurchaseTransactionEntryResponse
{
    public Guid Id { get; set; }

    public ProductResponse Product { get; set; }

    public decimal Price { get; set; }

    public PurchaseTransactionResponse PurchaseTransaction { get; set; }
}
