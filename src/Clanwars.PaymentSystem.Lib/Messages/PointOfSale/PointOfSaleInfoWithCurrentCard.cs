using Clanwars.PaymentSystem.Lib.Messages.Account;

namespace Clanwars.PaymentSystem.Lib.Messages.PointOfSale;

public record PointOfSaleInfoWithCurrentCard
{
    public string PosName { get; set; }
    public CardInfo CurrentCard { get; set; }
}
