using System;

namespace Clanwars.PaymentSystem.Lib.Messages.PointOfSale;

public record PointOfSaleResponse
{
    public Guid Id { get; set; }

    public string Name { get; set; }
}
