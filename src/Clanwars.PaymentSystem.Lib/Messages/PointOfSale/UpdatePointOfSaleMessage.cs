using System.ComponentModel.DataAnnotations;

namespace Clanwars.PaymentSystem.Lib.Messages.PointOfSale;

public record UpdatePointOfSaleMessage
{
    [Required(ErrorMessage = "The {0} field is required.")]
    [MinLength(1, ErrorMessage = "The field {0} must be a string or array type with a minimum length of '{1}'.")]
    [MaxLength(128, ErrorMessage = "The field {0} must be a string or array type with a maximum length of '{1}'.")]
    public string Name { get; set; }
}
