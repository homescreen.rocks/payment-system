namespace Clanwars.PaymentSystem.Lib.Messages.Hub;

public record Card
{
    public string Id { get; set; }

    public override string ToString()
    {
        return $"Card: {nameof(Id)}: {Id}";
    }
}
