using Clanwars.PaymentSystem.Lib.Enums;

namespace Clanwars.PaymentSystem.Lib.Messages.Hub;

public abstract record SubscriptionNotification
{
    public SubscriptionEventClasses EventClass { get; set; }
    public SubscriptionEventTypes EventType { get; set; }
}
