using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;

namespace Clanwars.PaymentSystem.Lib.Messages.Hub;

public record SubscriptionNotificationPointOfSaleInfo : SubscriptionNotification
{
    public PointOfSaleInfo Content { get; set; }
}
