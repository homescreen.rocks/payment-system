using System;

namespace Clanwars.PaymentSystem.Lib.Messages.ProductType;

public record ProductTypeResponse
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public int Order { get; set; }

    public string Image { get; set; }

    public bool Locked { get; set; }
}
