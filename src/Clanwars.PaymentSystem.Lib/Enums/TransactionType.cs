namespace Clanwars.PaymentSystem.Lib.Enums;

public enum TransactionType
{
    ChargeTransaction = 0,
    PurchaseTransaction = 1
}
