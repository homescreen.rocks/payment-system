using System.Collections.Generic;

namespace Clanwars.PaymentSystem.Lib;

public interface IPagedResult<T>
{
    int CurrentPage { get; }
    int PageCount { get; }
    int PageSize { get; }
    long TotalCount { get; }
    IList<T> Results { get; }
}
