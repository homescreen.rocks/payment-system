using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using System.Text.Json;
using Microsoft.Extensions.Options;

namespace Clanwars.OidcTokenAuthenticationClient;

public class OidcTokenAuthenticationClient : IOidcTokenAuthenticationClient
{
    private readonly HttpClient _httpClient;
    private readonly OidcOptions _options;
    private JwtSecurityToken? _currentJwtToken;

    public OidcTokenAuthenticationClient(IOptions<OidcOptions> options, HttpClient httpClient)
    {
        _options = options.Value;
        _httpClient = httpClient;
        _httpClient.DefaultRequestHeaders.Clear();
        _httpClient.DefaultRequestHeaders.Add("Cache-Control", "no-cache");
    }

    public async Task AddAuthorizationHeaderToHttpRequestAsync(HttpRequestMessage request)
    {
        if (_currentJwtToken is null || _currentJwtToken.ValidTo < DateTime.Now.AddSeconds(-30))
        {
            _currentJwtToken = await LoadTokenAsync();
        }

        request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _currentJwtToken.RawData);
    }

    public async Task<string> GetCurrentJwtTokenAsync()
    {
        if (_currentJwtToken is null || _currentJwtToken.ValidTo < DateTime.Now.AddSeconds(-30))
        {
            _currentJwtToken = await LoadTokenAsync();
        }

        return _currentJwtToken.RawData;
    }

    private async Task<JwtSecurityToken> LoadTokenAsync()
    {
        var form = new Dictionary<string, string>
        {
            {"grant_type", "client_credentials"},
            {"client_id", _options.ClientId},
            {"client_secret", _options.ClientSecret}
        };

        var requestMessage = new HttpRequestMessage(HttpMethod.Post, _options.TokenEndpoint);
        requestMessage.Content = new FormUrlEncodedContent(form);
        requestMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

        var response = await _httpClient.SendAsync(requestMessage).ConfigureAwait(false);
        Console.WriteLine(response.Content.ReadAsStringAsync().Result);
        response.EnsureSuccessStatusCode();

        await using var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);
        var tokenResponse = await JsonSerializer.DeserializeAsync<TokenResponseMessage>(responseStream).ConfigureAwait(false);

        return new JwtSecurityTokenHandler().ReadJwtToken(tokenResponse!.AccessToken);
    }
}
