using System;
using Clanwars.PaymentSystem.Backend.Entities;
using Microsoft.EntityFrameworkCore;

namespace Clanwars.PaymentSystem.Backend.Context;

public class PaymentContext : DbContext
{
    public PaymentContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<Product> Products { get; set; }
    public DbSet<ProductType> ProductTypes { get; set; }
    public DbSet<Account> Accounts { get; set; }
    public DbSet<PointOfSale> PointsOfSale { get; set; }

    public DbSet<PurchaseTransaction> PurchaseTransactions { get; set; }
    public DbSet<ChargeTransaction> ChargeTransactions { get; set; }
    public DbSet<Transaction> Transactions { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.UseCollation("utf8mb4_swedish_ci");
        modelBuilder.Entity<Transaction>()
            .HasOne(t => t.PointOfSale)
            .WithMany(pos => pos.Transactions)
            .OnDelete(DeleteBehavior.Restrict);

        modelBuilder.Entity<Account>()
            .Property(account => account.Balance)
            .HasDefaultValue(0.0m);

        modelBuilder.Entity<Account>()
            .HasIndex(account => account.CardId)
            .IsUnique();

        modelBuilder.Entity<Account>()
            .HasIndex(account => account.Email)
            .IsUnique();

        modelBuilder.Entity<ProductType>()
            .HasIndex(pt => pt.Name)
            .IsUnique();

        modelBuilder.Entity<ProductType>()
            .Property(pt => pt.Locked)
            .HasDefaultValue(false);

        modelBuilder.Entity<Product>()
            .HasIndex(p => p.Name)
            .IsUnique();

        modelBuilder.Entity<Product>()
            .Property(p => p.Locked)
            .HasDefaultValue(false);

        modelBuilder.Entity<PointOfSale>()
            .HasIndex(pos => pos.Name)
            .IsUnique();
    }
}
