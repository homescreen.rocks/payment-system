using System;
using System.Linq;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using FluentValidation;

namespace Clanwars.PaymentSystem.Backend.Models.BackendValidations;

public class CreateAccountValidator : AbstractValidator<CreateAccountMessage>
{
    private readonly IAccountRepository _accountRepository;

    public CreateAccountValidator(
        IAccountRepository accountRepository
    )
    {
        _accountRepository = accountRepository;

        RuleFor(cam => cam.Email)
            .Must(BeUniqueEmail())
            .WithMessage("The email must be unique");
        
        When(cam => cam.CardId != null, () =>
        {
            RuleFor(a => a.CardId)
                .Must(BeUniqueCardId())
                .WithMessage("The cardId must be unique");
        });
    }

    private Func<CreateAccountMessage, string, bool> BeUniqueEmail()
    {
        return (aur, email) =>
        {
            var account = _accountRepository
                .GetAccounts()
                .FirstOrDefault(a => a.Email == email);
            return account is null;
        };
    }

    private Func<CreateAccountMessage, string, bool> BeUniqueCardId()
    {
        return (aur, cardId) =>
        {
            var account = _accountRepository
                .GetAccounts()
                .FirstOrDefault(a => a.CardId == cardId);
            return account is null;
        };
    }
}
