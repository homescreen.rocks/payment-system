using System;
using System.Linq;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Lib.Messages.ProductType;
using FluentValidation;

namespace Clanwars.PaymentSystem.Backend.Models.BackendValidations;

public class CreateProductTypeValidator : AbstractValidator<CreateProductTypeMessage>
{
    private readonly IProductTypeRepository _productTypeRepository;

    public CreateProductTypeValidator(
        IProductTypeRepository productTypeRepository
    )
    {
        _productTypeRepository = productTypeRepository;

        RuleFor(cptm => cptm.Name)
            .Must(BeUniqueProductTypeName())
            .WithMessage("A product type with the name '{PropertyValue}' already exists.");
    }

    private Func<string, bool> BeUniqueProductTypeName()
    {
        return productTypeName =>
        {
            var productType = _productTypeRepository
                .GetProductTypes()
                .FirstOrDefault(pt => pt.Name == productTypeName);
            return productType is null;
        };
    }
}
