using System;
using System.Linq;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using FluentValidation;

namespace Clanwars.PaymentSystem.Backend.Models.BackendValidations;

public class CreatePointOfSaleValidator : AbstractValidator<CreatePointOfSaleMessage>
{
    private readonly IPointOfSaleRepository _pointOfSaleRepository;

    public CreatePointOfSaleValidator(
        IPointOfSaleRepository pointOfSaleRepository
    )
    {
        _pointOfSaleRepository = pointOfSaleRepository;

        RuleFor(cposm => cposm.Name)
            .Must(BeUniquePointOfSaleName())
            .WithMessage("A point of sale with the name '{PropertyValue}' already exists.");
    }

    private Func<string, bool> BeUniquePointOfSaleName()
    {
        return posName =>
        {
            var pos = _pointOfSaleRepository
                .GetPointsOfSale()
                .FirstOrDefault(pos => pos.Name == posName);
            return pos is null;
        };
    }
}
