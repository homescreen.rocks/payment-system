using System;
using System.Linq;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Backend.Services;
using Clanwars.PaymentSystem.Lib.Messages.Transaction;
using FluentValidation;

namespace Clanwars.PaymentSystem.Backend.Models.BackendValidations;

public class CreateTransactionValidator : AbstractValidator<CreateTransactionBaseMessage>
{
    private readonly IAccountRepository _accountRepository;
    private readonly IPointOfSaleService _pointOfSaleService;

    private const bool OnlyActives = true;

    public CreateTransactionValidator(
        IAccountRepository accountRepository,
        IPointOfSaleService pointOfSaleService
    )
    {
        _accountRepository = accountRepository;
        _pointOfSaleService = pointOfSaleService;

        RuleFor(ctbm => ctbm.AccountId)
            .Must(BeValidAccount())
            .WithMessage("An account with the id {PropertyValue} does not exist");

        RuleFor(ctbm => ctbm.PointOfSaleId)
            .Must(BeValidPointOfSale(OnlyActives))
            .WithMessage("An " + (OnlyActives ? "NFC ready " : "") + "point of sale with the id {PropertyValue} does not exist.");
    }

    private Func<Guid, bool> BeValidAccount()
    {
        return accountId =>
        {
            var account = _accountRepository
                .GetAccounts()
                .SingleOrDefault(a => a.Id == accountId);
            return account is not null;
        };
    }

    private Func<Guid, bool> BeValidPointOfSale(bool onlyActive = false)
    {
        return posId =>
        {
            var pos = _pointOfSaleService.GetPointOfSaleByIdAsync(posId).Result;

            if (pos is null)
            {
                return true;
            }

            if (onlyActive && pos.HasNfcClient)
            {
                return false;
            }

            return true;
        };
    }
}
