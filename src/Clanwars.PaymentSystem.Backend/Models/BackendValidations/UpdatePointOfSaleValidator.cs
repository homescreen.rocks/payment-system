using System;
using System.Linq;
using Clanwars.PaymentSystem.Backend.Repositories;
using FluentValidation;

namespace Clanwars.PaymentSystem.Backend.Models.BackendValidations;

public class UpdatePointOfSaleValidator : AbstractValidator<UpdatePointOfSaleRequest>
{
    private readonly IPointOfSaleRepository _pointOfSaleRepository;

    public UpdatePointOfSaleValidator(
        IPointOfSaleRepository pointOfSaleRepository
    )
    {
        _pointOfSaleRepository = pointOfSaleRepository;

        RuleFor(uposr => uposr.Name)
            .Must(BeUniquePointOfSaleName())
            .WithMessage("A point of sale with the name '{PropertyValue}' already exists.");
    }

    private Func<UpdatePointOfSaleRequest, string, bool> BeUniquePointOfSaleName()
    {
        return (message, posName) =>
        {
            var pos = _pointOfSaleRepository
                .GetPointsOfSale()
                .Where(pos => pos.Id != message.PosId)
                .FirstOrDefault(pos => pos.Name == posName);
            return pos is null;
        };
    }
}
