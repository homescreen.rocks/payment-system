using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Clanwars.PaymentSystem.Backend.ModelBinder;
using Clanwars.PaymentSystem.Lib.Messages.ProductType;

namespace Clanwars.PaymentSystem.Backend.Models;

public record UpdateProductTypeRequest : UpdateProductTypeMessage
{
    [JsonIgnore]
    [MapFromRoute]
    [Required]
    public Guid ProductTypeId { get; set; }
}
