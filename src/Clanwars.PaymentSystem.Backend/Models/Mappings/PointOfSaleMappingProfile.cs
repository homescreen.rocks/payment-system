using AutoMapper;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;

namespace Clanwars.PaymentSystem.Backend.Models.Mappings;

public class PointOfSaleMappingProfile : Profile
{
    public PointOfSaleMappingProfile()
    {
        CreateMap<Entities.PointOfSale, PointOfSaleResponse>();

        CreateMap<CreatePointOfSaleMessage, Entities.PointOfSale>()
            .ForMember(dest => dest.Id, opts => opts.Ignore());

        CreateMap<UpdatePointOfSaleMessage, Entities.PointOfSale>()
            .ForMember(dest => dest.Id, opts => opts.Ignore());
    }
}
