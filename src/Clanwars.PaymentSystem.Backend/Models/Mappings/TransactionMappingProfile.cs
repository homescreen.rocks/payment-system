using AutoMapper;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Lib.Messages.Transaction;

namespace Clanwars.PaymentSystem.Backend.Models.Mappings;

public class TransactionMappingProfile : Profile
{
    public TransactionMappingProfile()
    {
        CreateMap<PurchaseEntry, PurchaseTransactionEntry>()
            .ForMember(x => x.Id, opts => opts.Ignore())
            .ForMember(x => x.PurchaseTransaction, opts => opts.Ignore())
            .ForMember(x => x.Product, opts => opts.Ignore());

        CreateMap<ChargeTransaction, TransactionResponse>()
            .ForMember(x => x.Entries, opts => opts.Ignore());
        CreateMap<PurchaseTransaction, TransactionResponse>()
            .AfterMap((transaction, response) =>
                response.Entries.ForEach(entry =>
                {
                    entry.Product.Image = null;
                    entry.PurchaseTransaction = null;
                }));
        CreateMap<PurchaseTransactionEntry, PurchaseTransactionEntryResponse>();
        CreateMap<PurchaseTransaction, PurchaseTransactionResponse>();
    }
}
