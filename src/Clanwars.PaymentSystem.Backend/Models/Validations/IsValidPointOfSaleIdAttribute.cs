using System;
using System.ComponentModel.DataAnnotations;
using Clanwars.PaymentSystem.Backend.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Clanwars.PaymentSystem.Backend.Models.Validations;

public class IsValidPointOfSaleIdAttribute : ValidationAttribute
{
    public IsValidPointOfSaleIdAttribute(bool onlyActives = false)
    {
        OnlyActives = onlyActives;
    }

    public bool OnlyActives { get; }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        var posId = (Guid) value;
        var pointOfSaleService = validationContext.GetRequiredService<IPointOfSaleService>();
        var existingPointOfSale = pointOfSaleService.GetPointOfSaleByIdAsync(posId).Result;
        return existingPointOfSale == null
               || (OnlyActives && !existingPointOfSale.HasNfcClient)
            ? new ValidationResult(GetErrorMessage(posId))
            : ValidationResult.Success;
    }

    private string GetErrorMessage(Guid posId)
    {
        return "An " + (OnlyActives ? "NFC ready " : "") + $"point of sale with the id {posId} does not exist.";
    }
}
