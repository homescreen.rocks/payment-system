using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Clanwars.PaymentSystem.Backend.ModelBinder;
using Clanwars.PaymentSystem.Lib.Messages.Product;

namespace Clanwars.PaymentSystem.Backend.Models;

public record UpdateProductRequest : UpdateProductMessage
{
    [JsonIgnore]
    [MapFromRoute]
    [Required]
    public Guid ProductId { get; set; }
}
