using System;
using System.Collections.Generic;

namespace Clanwars.PaymentSystem.Backend.Helpers.OpenApi.Attributes;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
public class AdditionalSortableEntitiesAttribute : Attribute
{
    public List<string> AdditionSortableEntities { get; init; } = new();

    public AdditionalSortableEntitiesAttribute(params string[] sortableEntities)
    {
        AdditionSortableEntities.AddRange(sortableEntities);
    }
}
