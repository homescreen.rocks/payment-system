using System;
using System.Collections.Generic;

namespace Clanwars.PaymentSystem.Backend.Helpers.OpenApi.Attributes;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
public class AdditionalFilterableEntitiesAttribute : Attribute
{
    public List<string> AdditionFilterableEntities { get; init; } = new();

    public AdditionalFilterableEntitiesAttribute(params string[] filterableEntities)
    {
        AdditionFilterableEntities.AddRange(filterableEntities);
    }
}
