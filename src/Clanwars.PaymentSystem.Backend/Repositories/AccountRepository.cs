using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Context;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Helpers.Sieve;
using Clanwars.PaymentSystem.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.PaymentSystem.Backend.Repositories;

public class AccountRepository : IAccountRepository
{
    private readonly PaymentContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;

    public AccountRepository(
        PaymentContext context,
        ISieveProcessor sieveProcessor,
        IOptions<SieveOptions> sieveOptions
    )
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<Account> GetAccounts()
    {
        var result = _context
            .Accounts;
        return result;
    }

    public async Task<PagedResult<Account>> GetAccountsAsync(SieveModel sieveModel)
    {
        var result = GetAccounts()
            .AsNoTrackingWithIdentityResolution();

        return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task<Account> GetAccountByEmailAsync(string email)
    {
        var result = GetAccounts()
            .Where(account => account.Email == email);

        return await result.SingleOrDefaultAsync();
    }

    public async Task<Account> GetAccountInformationByIdAsync(Guid accountId)
    {
        var result = GetAccounts()
            .Where(account => account.Id == accountId);

        return await result.SingleOrDefaultAsync();
    }

    public async Task<Account> GetAccountInformationByCardIdAsync(string cardId)
    {
        if (cardId == null)
        {
            return null;
        }

        var result = GetAccounts()
            .Where(account => account.CardId == cardId);

        return await result.SingleOrDefaultAsync();
    }

    public async Task CreateAccountAsync(Account account)
    {
        await _context.Accounts.AddAsync(account);
        await _context.SaveChangesAsync();
    }

    public Task UpdateAccountAsync(Account account)
    {
        return _context.SaveChangesAsync();
    }

    public async Task DeleteAccountAsync(Account account)
    {
        _context.Accounts.Remove(account);
        await _context.SaveChangesAsync();
    }
}
