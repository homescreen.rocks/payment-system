using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Context;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Helpers.Sieve;
using Clanwars.PaymentSystem.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.PaymentSystem.Backend.Repositories;

public class ProductTypeRepository : IProductTypeRepository
{
    private readonly PaymentContext _context;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly SieveOptions _sieveOptions;

    public ProductTypeRepository(
        PaymentContext context,
        ISieveProcessor sieveProcessor,
        IOptions<SieveOptions> sieveOptions
    )
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<ProductType> GetProductTypes()
    {
        var result = _context
            .ProductTypes;
        return result;
    }

    public async Task<PagedResult<ProductType>> GetProductTypesAsync(SieveModel sieveModel)
    {
        var result = GetProductTypes()
            .AsNoTrackingWithIdentityResolution();
        return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task<ProductType> GetProductTypeByIdAsync(Guid productTypeId)
    {
        var result = GetProductTypes()
            .Where(productType => productType.Id == productTypeId);

        return await result.SingleOrDefaultAsync();
    }

    public async Task<ProductType> GetProductTypeByNameAsync(string productTypeName)
    {
        var result = GetProductTypes()
            .Where(productType => productType.Name == productTypeName);

        return await result.SingleOrDefaultAsync();
    }

    public async Task CreateProductTypeAsync(ProductType createProductType)
    {
        await _context.ProductTypes.AddAsync(createProductType);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateProductTypeAsync(ProductType updateProductType)
    {
        await _context.SaveChangesAsync();
    }
}
