using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Lib;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Repositories;

public interface ITransactionRepository
{
    IQueryable<Transaction> GetTransactions();
    Task<PagedResult<Transaction>> GetTransactionsAsync(SieveModel sieveModel);
    Task<Transaction> GetTransactionByIdAsync(Guid transactionId);
    Task<bool> WriteNewTransactionAsync(Transaction transaction);
    Task<bool> CancelTransactionAsync(Transaction transaction);
}
