using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Context;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Helpers.Sieve;
using Clanwars.PaymentSystem.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.PaymentSystem.Backend.Repositories;

public class ProductRepository : IProductRepository
{
    private readonly PaymentContext _context;
    private readonly SieveOptions _sieveOptions;
    private readonly ISieveProcessor _sieveProcessor;
    private readonly ILogger<ProductRepository> _logger;

    public ProductRepository(
        PaymentContext context,
        IOptions<SieveOptions> sieveOptions,
        ILogger<ProductRepository> logger,
        ISieveProcessor sieveProcessor
    )
    {
        _context = context;
        _sieveOptions = sieveOptions.Value;
        _logger = logger;
        _sieveProcessor = sieveProcessor;
    }

    public IQueryable<Product> GetProducts()
    {
        var result = _context
            .Products
            .Include(p => p.ProductType);
        return result;
    }

    public async Task<PagedResult<Product>> GetProductsAsync(SieveModel sieveModel)
    {
        var result = GetProducts()
            .AsNoTrackingWithIdentityResolution();
        return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task<Product> GetProductByIdAsync(Guid productId)
    {
        var result = GetProducts()
            .Where(product => product.Id == productId);

        return await result.SingleOrDefaultAsync();
    }

    public async Task<Product> GetProductByNameAsync(string productName)
    {
        var result = GetProducts()
            .Where(product => product.Name == productName);

        return await result.SingleOrDefaultAsync();
    }

    public async Task CreateProductAsync(Product newProduct)
    {
        _context.Products.Add(newProduct);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateProductAsync(Product updateProduct)
    {
        await _context.SaveChangesAsync();
    }
}
