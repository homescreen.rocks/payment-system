using System;
using System.Linq;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Backend.Context;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Helpers.Sieve;
using Clanwars.PaymentSystem.Lib;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Sieve.Models;
using Sieve.Services;

namespace Clanwars.PaymentSystem.Backend.Repositories;

public class PointOfSaleRepository : IPointOfSaleRepository
{
    private readonly PaymentContext _context;
    private readonly SieveOptions _sieveOptions;
    private readonly ISieveProcessor _sieveProcessor;

    public PointOfSaleRepository(
        PaymentContext context,
        ISieveProcessor sieveProcessor,
        IOptions<SieveOptions> sieveOptions
    )
    {
        _context = context;
        _sieveProcessor = sieveProcessor;
        _sieveOptions = sieveOptions.Value;
    }

    public IQueryable<PointOfSale> GetPointsOfSale()
    {
        var result = _context
            .PointsOfSale;
        return result;
    }

    public async Task<PagedResult<PointOfSale>> GetPointsOfSaleAsync(SieveModel sieveModel)
    {
        var result = GetPointsOfSale()
            .AsNoTrackingWithIdentityResolution();
        return await _sieveProcessor.GetPagedAsync(result, _sieveOptions, sieveModel);
    }

    public async Task<PointOfSale> GetPointOfSaleByIdAsync(Guid posId)
    {
        var result = GetPointsOfSale()
            .Where(pos => pos.Id == posId);

        return await result.SingleOrDefaultAsync();
    }

    public async Task<PointOfSale> GetPointOfSaleByNameAsync(string posName)
    {
        var result = GetPointsOfSale()
            .Where(pos => pos.Name == posName);

        return await result.SingleOrDefaultAsync();
    }

    public async Task CreatePointOfSaleAsync(PointOfSale newPointOfSale)
    {
        await _context.PointsOfSale.AddAsync(newPointOfSale);
        await _context.SaveChangesAsync();
    }

    public async Task UpdatePointOfSaleAsync(PointOfSale updatePointOfSale)
    {
        await _context.SaveChangesAsync();
    }
}
