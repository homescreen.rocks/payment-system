﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Models;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Backend.Services;
using Clanwars.PaymentSystem.Lib;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Controllers;

[Authorize(Roles = "Admin")]
[Route("api/v1/[controller]")]
[ApiController]
public class AccountsController : ControllerBase
{
    private readonly IAccountsService _accountsService;
    private readonly IMapper _mapper;
    private readonly ITransactionRepository _transactionRepository;

    public AccountsController(
        IAccountsService accountsService,
        IMapper mapper,
        ITransactionRepository transactionRepository
    )
    {
        _accountsService = accountsService;
        _mapper = mapper;
        _transactionRepository = transactionRepository;
    }

    // GET api/v1/accounts
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<AccountInfo>), StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<AccountInfo>>> GetAccounts(
        [FromQuery] SieveModel sieveModel
    )
    {
        var result = await _accountsService.GetAsync(sieveModel);
        return Ok(_mapper.Map<PagedResult<AccountInfo>>(result));
    }

    // POST api/v1/accounts
    [HttpPost]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(typeof(AccountInfo), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<AccountInfo>> CreateAccount(
        [FromBody] CreateAccountMessage accountMessage
    )
    {
        var acc = _mapper.Map<Account>(accountMessage);
        acc.Id = Guid.NewGuid();
        await _accountsService.CreateAsync(acc);
        return CreatedAtAction(nameof(GetAccounts), new {filters = $"id=={acc.Id}"}, _mapper.Map<AccountInfo>(acc));
    }

    // PUT api/v1/accounts/{accountId}
    [HttpPut("{accountId:guid}")]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<AccountInfo>> UpdateAccount(
        [FromBody] UpdateAccountRequest updateAccount
    )
    {
        var account = await _accountsService.GetByIdAsync(updateAccount.AccountId);

        if (account is null)
        {
            return NotFound();
        }

        _mapper.Map(updateAccount, account);

        await _accountsService.UpdateAsync(account);

        return Ok(_mapper.Map<AccountInfo>(account));
    }

    // PATCH api/v1/accounts/{accountId}/lock
    [HttpPatch("{accountId:guid}/lock")]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(typeof(AccountInfo), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<IActionResult> LockAccount(
        [FromRoute] Guid accountId
    )
    {
        var account = await _accountsService.GetByIdAsync(accountId);

        if (account is null)
        {
            return NotFound();
        }

        if (account.Locked)
        {
            return Conflict();
        }

        account.Locked = true;
        await _accountsService.UpdateAsync(account);

        return Ok(_mapper.Map<AccountInfo>(account));
    }

    // PATCH api/v1/accounts/{accountId}/unlock
    [HttpPatch("{accountId:guid}/unlock")]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(typeof(AccountInfo), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<IActionResult> UnlockAccount(
        [FromRoute] Guid accountId
    )
    {
        var account = await _accountsService.GetByIdAsync(accountId);
        if (account is null)
        {
            return NotFound();
        }

        if (!account.Locked)
        {
            return Conflict();
        }

        account.Locked = false;
        await _accountsService.UpdateAsync(account);

        return Ok(_mapper.Map<AccountInfo>(account));
    }

    // DELETE api/v1/accounts/{accounId}
    [Authorize(Roles = "Admin")]
    [HttpDelete("{accountId:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status423Locked)]
    public async Task<ActionResult> DeleteAccount(
        [FromRoute] Guid accountId
    )
    {
        var account = await _accountsService.GetByIdAsync(accountId);

        if (account is null)
        {
            return NotFound();
        }

        var accountSieveFilter = new SieveModel {Filters = $"account.id=={accountId}"};
        var transactions = await _transactionRepository.GetTransactionsAsync(accountSieveFilter);

        if (transactions.Results.Any(t => t.GetType() == typeof(PurchaseTransaction)))
        {
            var errorMsg = new ProblemDetails
            {
                Type = "payment/active-user-transactions",
                Title = "Active User",
                Detail = "You can't delete a user who has (already) some transactions in his history.",
                Instance = HttpContext.Request.Path
            };
            return StatusCode(StatusCodes.Status423Locked, errorMsg);
        }

        if (account.Balance > 0)
        {
            var errorMsg = new ProblemDetails
            {
                Type = "payment/active-user-balance",
                Title = "Active User",
                Detail = "You can't delete a user who has still some money on his account.",
                Instance = HttpContext.Request.Path
            };
            return StatusCode(StatusCodes.Status423Locked, errorMsg);
        }

        await _accountsService.DeleteAsync(account);

        return NoContent();
    }
}
