using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Hubs;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Backend.Services;
using Clanwars.PaymentSystem.Lib;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.Transaction;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Controllers;

[Authorize(Roles = "Admin,CashDesk,User")]
[Route("api/v1/[controller]")]
[ApiController]
public class TransactionsController : ControllerBase
{
    private readonly IConnectionService _connectionService;
    private readonly IMapper _mapper;
    private readonly IAccountRepository _accountRepository;
    private readonly ITransactionRepository _transactionRepository;
    private readonly IHubContext<PaymentHub, IPaymentHub> _paymentHubContext;

    public TransactionsController(
        IConnectionService connectionService,
        IAccountRepository accountRepository,
        ITransactionRepository transactionRepository,
        IMapper mapper,
        IHubContext<PaymentHub, IPaymentHub> paymentHubContext
    )
    {
        _connectionService = connectionService;
        _mapper = mapper;
        _paymentHubContext = paymentHubContext;
        _accountRepository = accountRepository;
        _transactionRepository = transactionRepository;
    }

    // GET /api/v1/Transactions
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<TransactionResponse>), StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<TransactionResponse>>> ListTransactions(
        [FromQuery] SieveModel sieveModel
    )
    {
        if (!(User.IsInRole("Admin") || User.IsInRole("CashDesk")))
        {
            if (!string.IsNullOrEmpty(sieveModel.Filters))
            {
                sieveModel.Filters += ",";
            }

            sieveModel.Filters += $"account.id=={User.Identity.Name}";
        }

        var transactionsResult = await _transactionRepository.GetTransactionsAsync(sieveModel);
        return Ok(_mapper.Map<PagedResult<TransactionResponse>>(transactionsResult));
    }

    //POST /api/v1/Transactions/Purchase
    [Authorize(Roles = "Admin,CashDesk")]
    [HttpPost("Purchase")]
    [ProducesResponseType(typeof(CardInfo), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status402PaymentRequired)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult<CardInfo>> Purchase(
        [FromBody] CreatePurchaseTransactionMessage createPurchaseTransactionMessage
    )
    {
        // check card on nfc reader
        var card = await _connectionService.GetCardAsync(createPurchaseTransactionMessage.PointOfSaleId);

        // check account from reader vs card
        if (card?.Account.Id != createPurchaseTransactionMessage.AccountId)
        {
            return Conflict();
        }

        // check account balance
        var totalCartBalance = createPurchaseTransactionMessage.Entries.Sum(entry => entry.Price);
        if (totalCartBalance > (card.Account.Balance + card.Account.Overdraft))
        {
            // not enough money
            var errorMsg = new ProblemDetails
            {
                Type = "payment/out-of-credit",
                Title = "Out of credit",
                Detail = "Not enough cash in the täsch!",
                Instance = HttpContext.Request.Path
            };

            return StatusCode(StatusCodes.Status402PaymentRequired, errorMsg);
        }

        // checkout cart
        var purchaseTransaction = new PurchaseTransaction
        {
            Id = Guid.NewGuid(),
            AccountId = card.Account.Id,
            PointOfSaleId = createPurchaseTransactionMessage.PointOfSaleId,
            Sum = totalCartBalance,
            Timestamp = DateTime.Now,
            Comment = createPurchaseTransactionMessage.Comment,
            Entries = _mapper.Map<List<PurchaseTransactionEntry>>(createPurchaseTransactionMessage.Entries)
        };

        purchaseTransaction.Entries.ForEach(e => e.Id = Guid.NewGuid());
        var transactionSuccess = await _transactionRepository.WriteNewTransactionAsync(purchaseTransaction);

        if (!transactionSuccess)
        {
            // something went wrong
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        // return updated cardInfo
        var updatedCardInfo =
            _mapper.Map<CardInfo>(await _accountRepository.GetAccountInformationByIdAsync(card.Account.Id));
        await _paymentHubContext.Clients.Group(purchaseTransaction.PointOfSaleId.ToString())
            .NewCard(updatedCardInfo);
        return Ok(updatedCardInfo);
    }

    // POST /api/v1/Transactions/Charge
    [Authorize(Roles = "Admin,CashDesk")]
    [HttpPost("Charge")]
    [ProducesResponseType(typeof(CardInfo), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult<CardInfo>> Charge(
        [FromBody] CreateChargeTransactionMessage createChargeTransactionMessage
    )
    {
        // check card on nfc reader
        var card = await _connectionService.GetCardAsync(createChargeTransactionMessage.PointOfSaleId);

        // check account from reader vs card
        if (card?.Account.Id != createChargeTransactionMessage.AccountId)
        {
            return Conflict();
        }

        // charge amount to account
        var chargeTransaction = new ChargeTransaction
        {
            Id = Guid.NewGuid(),
            AccountId = card.Account.Id,
            PointOfSaleId = createChargeTransactionMessage.PointOfSaleId,
            Sum = createChargeTransactionMessage.Amount,
            Comment = createChargeTransactionMessage.Comment,
            Timestamp = DateTime.Now
        };
        var transactionSuccess = await _transactionRepository.WriteNewTransactionAsync(chargeTransaction);

        if (!transactionSuccess)
        {
            // something went wrong
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        // return updated cardInfo
        var updatedCardInfo =
            _mapper.Map<CardInfo>(await _accountRepository.GetAccountInformationByIdAsync(card.Account.Id));
        await _paymentHubContext.Clients.Group(chargeTransaction.PointOfSaleId.ToString())
            .NewCard(updatedCardInfo);
        return Ok(updatedCardInfo);
    }

    // DELETE /api/v1/Transactions/{transactionId}
    [Authorize(Roles = "Admin,CashDesk")]
    [HttpDelete("{transactionId}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status423Locked)]
    public async Task<ActionResult> Cancel(
        [FromRoute] Guid transactionId
    )
    {
        var transaction = await _transactionRepository.GetTransactionByIdAsync(transactionId);
        if (transaction is null)
        {
            return NotFound();
        }

        if (transaction.Cancelled != null || DateTime.Now.Subtract(transaction.Timestamp) > TimeSpan.FromMinutes(10))
        {
            return StatusCode(StatusCodes.Status423Locked);
        }

        await _transactionRepository.CancelTransactionAsync(transaction);

        var updatedCardInfo =
            _mapper.Map<CardInfo>(await _accountRepository.GetAccountInformationByIdAsync(transaction.Account.Id));
        await _paymentHubContext.Clients.Group(transaction.PointOfSaleId.ToString())
            .NewCard(updatedCardInfo);

        return NoContent();
    }
}
