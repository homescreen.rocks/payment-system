using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Models;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Lib;
using Clanwars.PaymentSystem.Lib.Messages.Product;
using Clanwars.PaymentSystem.Lib.Messages.ProductType;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Controllers;

[Authorize(Roles = "Admin,CashDesk")]
[Route("api/v1/[controller]")]
[ApiController]
public class ProductTypesController : ControllerBase
{
    private readonly IProductTypeRepository _productTypeRepository;
    private readonly IMapper _mapper;
    private readonly ILogger<ProductTypesController> _logger;

    public ProductTypesController(
        IProductTypeRepository productTypeRepository,
        IMapper mapper,
        ILogger<ProductTypesController> logger
    )
    {
        _productTypeRepository = productTypeRepository;
        _mapper = mapper;
        _logger = logger;
    }

    // GET api/producttypes
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<ProductType>), StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<ProductType>>> GetProductTypes(
        [FromQuery] SieveModel sieveModel
    )
    {
        var productTypesResult = await _productTypeRepository.GetProductTypesAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<ProductTypeResponse>>(productTypesResult));
    }

    // GET api/v1/producttypes/{productTypeId}/image
    [HttpGet("{productTypeId:guid}/image")]
    [AllowAnonymous]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult> GetProductImage(
        [FromRoute] Guid productTypeId
    )
    {
        var productType = await _productTypeRepository.GetProductTypeByIdAsync(productTypeId);

        if (productType is null)
        {
            return NotFound();
        }

        if (productType.Image is null)
        {
            return NotFound();
        }
        
        var match = Regex.Match(productType.Image, @"data:(?<type>.+?);base64,(?<data>.+)");
        if (!match.Success)
        {
            return NotFound();
        }

        var base64Data = match.Groups["data"].Value;
        var mimeType = match.Groups["type"].Value;
        var binData = Convert.FromBase64String(base64Data);

        return File(binData, mimeType);
    }

    // POST api/v1/producttypes
    [HttpPost]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<ProductResponse>> CreateProductType(
        [FromBody] CreateProductTypeMessage createProductTypeMessage
    )
    {
        var productType = _mapper.Map<ProductType>(createProductTypeMessage);
        productType.Id = Guid.NewGuid();

        await _productTypeRepository.CreateProductTypeAsync(productType);

        return CreatedAtAction(nameof(GetProductTypes), new { filters = $"id=={productType.Id}" }, productType);
    }

    // PUT api/v1/products/{productTypeId}
    [HttpPut("{productTypeId:guid}")]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<ProductType>> UpdateProductType(
        [FromBody] UpdateProductTypeRequest updateProductTypeRequest
    )
    {
        var productType = await _productTypeRepository.GetProductTypeByIdAsync(updateProductTypeRequest.ProductTypeId);

        if (productType is null)
        {
            return NotFound();
        }

        _mapper.Map(updateProductTypeRequest, productType);

        await _productTypeRepository.UpdateProductTypeAsync(productType);
        return Ok(_mapper.Map<ProductTypeResponse>(productType));
    }
}
