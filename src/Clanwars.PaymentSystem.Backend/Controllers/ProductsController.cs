﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Models;
using Clanwars.PaymentSystem.Backend.Models.Validations;
using Clanwars.PaymentSystem.Backend.Repositories;
using Clanwars.PaymentSystem.Lib;
using Clanwars.PaymentSystem.Lib.Messages.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Controllers;

[Authorize(Roles = "Admin,CashDesk")]
[Route("api/v1/[controller]")]
[ApiController]
public class ProductsController : ControllerBase
{
    private readonly IProductRepository _productRepository;
    private readonly IMapper _mapper;
    private readonly ILogger<ProductsController> _logger;

    public ProductsController(
        IProductRepository productRepository,
        IMapper mapper,
        ILogger<ProductsController> logger
    )
    {
        _productRepository = productRepository;
        _mapper = mapper;
        _logger = logger;
    }

    // GET api/v1/products
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<ProductResponse>>> GetProducts(
        [FromQuery] SieveModel sieveModel
    )
    {
        var productsResult = await _productRepository.GetProductsAsync(sieveModel);

        return Ok(_mapper.Map<PagedResult<ProductResponse>>(productsResult));
    }

    // GET api/v1/products/{productId}/image
    [HttpGet("{productId:guid}/image")]
    [AllowAnonymous]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult> GetProductImage(
        [FromRoute] [IsValidProductId] Guid productId
    )
    {
        var product = await _productRepository.GetProductByIdAsync(productId);

        if (product is null)
        {
            return NotFound();
        }

        if (product.Image is null)
        {
            return NotFound();
        }

        var match = Regex.Match(product.Image, @"data:(?<type>.+?);base64,(?<data>.+)");
        if (!match.Success)
        {
            return NotFound();
        }

        var base64Data = match.Groups["data"].Value;
        var mimeType = match.Groups["type"].Value;
        var binData = Convert.FromBase64String(base64Data);

        return File(binData, mimeType);
    }

    // POST api/v1/products
    [HttpPost]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<ProductResponse>> CreateProduct(
        [FromBody] CreateProductMessage createProductMessage
    )
    {
        var product = _mapper.Map<Product>(createProductMessage);
        product.Id = Guid.NewGuid();

        await _productRepository.CreateProductAsync(product);

        return CreatedAtAction(nameof(GetProducts), new { filters = $"id=={product.Id}" }, _mapper.Map<ProductResponse>(product));
    }

    // PUT api/v1/products/{productId}
    [HttpPut("{productId:guid}")]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<ProductResponse>> UpdateProduct(
        [FromBody] UpdateProductRequest updateProductRequest
    )
    {
        var product = await _productRepository.GetProductByIdAsync(updateProductRequest.ProductId);

        if (product is null)
        {
            return NotFound();
        }

        _mapper.Map(updateProductRequest, product);

        await _productRepository.UpdateProductAsync(product);

        return Ok(_mapper.Map<ProductResponse>(product));
    }
}
