﻿using AutoMapper;
using Clanwars.PaymentSystem.Backend.Options;
using Clanwars.PaymentSystem.Lib.Messages.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Clanwars.PaymentSystem.Backend.Controllers;

[Route("api/v1/.configuration")]
[ApiController]
public class ConfigController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly AuthOptions _authOptions;

    public ConfigController(
        IMapper mapper,
        IOptions<AuthOptions> authOptions)
    {
        _mapper = mapper;
        _authOptions = authOptions.Value;
    }

    // GET api/v1/accounts
    [HttpGet("frontend")]
    [ProducesResponseType(typeof(ConfigurationResponse), StatusCodes.Status200OK)]
    public ActionResult<ConfigurationResponse> GetFrontendConfiguration()
    {
        var result = new ConfigurationResponse
        {
            Auth = _mapper.Map<ConfigurationAuthResponse>(_authOptions)
        };
        return Ok(result);
    }
}
