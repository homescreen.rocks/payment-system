using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Clanwars.PaymentSystem.Backend.Entities;
using Clanwars.PaymentSystem.Backend.Hubs;
using Clanwars.PaymentSystem.Backend.Models.Validations;
using Clanwars.PaymentSystem.Backend.Services;
using Clanwars.PaymentSystem.Lib.Enums;
using Clanwars.PaymentSystem.Lib.Messages.Hub;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Sieve.Models;

namespace Clanwars.PaymentSystem.Backend.Controllers;

[Authorize(Roles = "Admin,CashDesk")]
[Route("api/v1/[controller]")]
[ApiController]
public class PointsOfSaleController : ControllerBase
{
    private readonly IPointOfSaleService _pointOfSaleService;
    private readonly IConnectionService _connectionService;
    private readonly IMapper _mapper;
    private readonly ILogger<PointsOfSaleController> _logger;
    private readonly IHubContext<PaymentHub, IPaymentHub> _paymentHubContext;

    public PointsOfSaleController(
        IPointOfSaleService pointOfSaleService,
        IMapper mapper,
        ILogger<PointsOfSaleController> logger,
        IHubContext<PaymentHub, IPaymentHub> paymentHubContext, IConnectionService connectionService)
    {
        _pointOfSaleService = pointOfSaleService;
        _mapper = mapper;
        _logger = logger;
        _paymentHubContext = paymentHubContext;
        _connectionService = connectionService;
    }

    // GET api/v1/PointsOfSale
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<ActionResult<IEnumerable<PointOfSaleInfo>>> GetPointsOfSale([FromQuery] SieveModel sieveModel)
    {
        var pointOfSaleResult = await _pointOfSaleService.GetPointsOfSaleAsync(sieveModel);

        return Ok(pointOfSaleResult);
    }

    // POST api/v1/PointsOfSale
    [HttpPost]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<PointOfSaleInfo>> CreatePointOfSale(
        [FromBody] CreatePointOfSaleMessage createPointOfSaleMessage)
    {
        var pointOfSale = _mapper.Map<PointOfSale>(createPointOfSaleMessage);
        pointOfSale.Id = Guid.NewGuid();

        var newPos = await _pointOfSaleService.CreatePointOfSaleAsync(pointOfSale);

        await _paymentHubContext.Clients.Group("EventSubscriber").NewEvent(
            new SubscriptionNotificationPointOfSaleInfo
            {
                EventClass = SubscriptionEventClasses.PointOfSaleInfo,
                EventType = SubscriptionEventTypes.PosAdded,
                Content = newPos
            });

        return CreatedAtAction(nameof(GetPointsOfSale), new {filters = $"id=={newPos.Id}"}, newPos);
    }

    // PUT api/v1/PointsOfSale/{id}
    [HttpPut("{id:guid}")]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<PointOfSaleInfo>> UpdatePointOfSale(
        [FromBody] UpdatePointOfSaleMessage updatePointOfSaleMessage, [FromRoute] [IsValidPointOfSaleId] Guid id)
    {
        var pointOfSale = _mapper.Map<PointOfSale>(updatePointOfSaleMessage);
        pointOfSale.Id = id;

        var pos = await _pointOfSaleService.UpdatePointOfSaleAsync(pointOfSale);

        await _paymentHubContext.Clients.Group("EventSubscriber").NewEvent(
            new SubscriptionNotificationPointOfSaleInfo
            {
                EventClass = SubscriptionEventClasses.PointOfSaleInfo,
                EventType = SubscriptionEventTypes.PosConfigured,
                Content = pos
            });

        return Ok(pos);
    }

    [HttpGet("current-cards")]
    [Authorize(Roles = "Admin")]
    [ProducesResponseType(typeof(IList<PointOfSaleInfoWithCurrentCard>), StatusCodes.Status200OK)]
    public async Task<ActionResult<IList<PointOfSaleInfoWithCurrentCard>>> GetCurrentCards()
    {
        var currentPos = await _pointOfSaleService.GetPointsOfSale().ToListAsync();
        var posesWithCards = currentPos.Select(p =>
        {
            var currentCard = _connectionService.GetCardAsync(p.Id).Result;
            var posWithCard = new PointOfSaleInfoWithCurrentCard
            {
                PosName = p.Name,
                CurrentCard = currentCard
            };
            return posWithCard;
        });
        return Ok(posesWithCards);
    }
}
