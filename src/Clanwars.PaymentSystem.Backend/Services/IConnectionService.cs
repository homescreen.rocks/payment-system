using System;
using System.Threading.Tasks;
using Clanwars.PaymentSystem.Lib.Enums;
using Clanwars.PaymentSystem.Lib.Messages.Account;
using Clanwars.PaymentSystem.Lib.Messages.PointOfSale;

namespace Clanwars.PaymentSystem.Backend.Services;

public interface IConnectionService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="posId"></param>
    /// <param name="clientType"></param>
    /// <returns></returns>
    /// <exception cref="InvalidOperationException">Is thrown if there are multiple connections registered for one PointOfSale</exception>
    string GetConnection(Guid posId, PosClientType clientType);

    bool SetConnection(Guid posId, PosClientType clientType, string connectionId);

    (Guid posId, PosClientType posClientType)? RemoveConnection(string connection);

    Guid? GetPointOfSaleId(string connection);
        
    bool PosIsReady(Guid posId);

    // NFC specific methods
    Task<CardInfo> GetCardAsync(Guid posId);
    void PublishResponseFromNfc(string connectionId, CardInfo cardInfo);
}