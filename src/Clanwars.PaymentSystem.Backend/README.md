# Payment-System (Backend)

## RUN

1. `cd docs/docker`
2. `sudo docker-compose -f [-H <server_address>] payment-backend.yml up` (REST interface of docker has to be enabled)
3. now database is running
4. open new terminal
5. `dotnet user-secrets set "Database::Password" "<Password>"`
6. `dotnet ef database update`

## API

* REST is documented with Swagger: https://localhost:5001/swagger/
* Roles as documented in [docs/roles.md](docs/roles.md)
* SignalR methods are described in sequence charts

## Message sequence charts

* dotted lines use websockets for communication
* solid lines use HTTP for communication

### Obtain authorization token

```mermaid
sequenceDiagram
    participant a as (any)
    participant Backend
    Note over a,Backend: api/v1/login
    a ->> Backend: [LoginRequest]
    Backend ->> a: [LoginResponse]
```

### Join nfc client into point of sale

```mermaid
sequenceDiagram
    participant NFC
    participant Backend
    participant CashDesk
    NFC -->> Backend: JoinPointOfSale[PointOfSaleDevice]
    Backend -->> CashDesk: NewCardReader[PointOfSaleInfo]
    Backend -->> NFC: Welcome[PointOfSaleInfo]
```

### Join cach desk into point of sale

```mermaid
sequenceDiagram
    participant NFC
    participant Backend
    participant CashDesk
    CashDesk -->> Backend: JoinPointOfSale[PointOfSaleDevice]
    Backend -->> NFC: NewCashDesk[PointOfSaleInfo]
    Backend -->> CashDesk: Welcome[PointOfSaleInfo]
```

### Put card to reader

```mermaid
sequenceDiagram
    participant NFC
    participant Backend
    participant CashDesk
    NFC -->> Backend: NewCard[Card]
    Backend -->> CashDesk: NewCard[CardInfo]
    Backend -->> NFC: NewCard[CardInfo]
```

### Remove card from reader

```mermaid
sequenceDiagram
    participant NFC
    participant Backend
    participant CashDesk
    NFC -->> Backend: RemovedCard[null]
    Backend -->> CashDesk: RemovedCard[null]
```

### Successful purchase

```mermaid
sequenceDiagram
    participant NFC
    participant Backend
    participant CashDesk
    CashDesk ->> Backend: CreatePurchaseTransaction[Entries[],AccountId]
    activate Backend
    Backend -->> NFC: GetCard[null]
    NFC -->> Backend: CurrentCard[Card]
    Backend ->> CashDesk: PurchaseSuccessful[CardInfo], HTTP 200
    deactivate Backend 
```

### Purchase errors

#### Missing/Wrong Card

```mermaid
sequenceDiagram
    participant NFC
    participant Backend
    participant CashDesk
    CashDesk ->> Backend: CreatePurchaseTransaction[Entries[],AccountId]
    activate Backend
    Backend -->> NFC: GetCard[null]
    NFC -->> Backend: CurrentCard[Card]
    note over Backend: Error:<br>missing/wrong card
    Backend ->> CashDesk: MissingCard[null], HTTP 409
    deactivate Backend 
```

#### Not enough money

```mermaid
sequenceDiagram
    participant NFC
    participant Backend
    participant CashDesk
    CashDesk ->> Backend: CreatePurchaseTransaction[Entries[],AccountId]
    activate Backend
    Backend -->> NFC: GetCard[null]
    NFC -->> Backend: CurrentCard[Card]
    note over Backend: Error:<br>not enough credit
    Backend ->> CashDesk: MissingCash[null], HTTP 402
    deactivate Backend 
```

### Successful charge

```mermaid
sequenceDiagram
    participant NFC
    participant Backend
    participant CashDesk
    CashDesk ->> Backend: CreateChargeTransaction[Amount,AccountId]
    activate Backend
    Backend -->> NFC: GetCard[null]
    NFC -->> Backend: CurrentCard[Card]
    Backend ->> CashDesk: ChargeSuccessful[CardInfo], HTTP 200
    deactivate Backend 
```

### Charge errors

#### Missing/Wrong Card

```mermaid
sequenceDiagram
    participant NFC
    participant Backend
    participant CashDesk
    CashDesk ->> Backend: CreateChargeTransaction[Amount,AccountId]
    activate Backend
    Backend -->> NFC: GetCard[null]
    NFC -->> Backend: CurrentCard[Card]
    note over Backend: Error:<br>missing/wrong card
    Backend ->> CashDesk: MissingCard[null], HTTP 409
    deactivate Backend 
```
