namespace Clanwars.PaymentSystem.NfcClient.Models;

public enum MethodTypes
{
    Welcome = 0,
    JoinRequired,
    NewCashDesk,
    DeviceLeftPos,
    GetCard,
    JoinPointOfSale = 100,
    NewCard,
    CurrentCard,
    RemovedCard,
    Error = 400
}