using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PCSC;
using PCSC.Exceptions;
using PCSC.Iso7816;
using PCSC.Monitoring;
using PCSC.Utils;

namespace Clanwars.PaymentSystem.NfcClient.Services;

public class PcScService : IPcScService
{
    private readonly IContextFactory _contextFactory = ContextFactory.Instance;

    private readonly ILogger<PcScService> _logger;
    private readonly ClientSettings _config;
    private readonly IServiceProvider _services;

    private string _currentCardId;

    private string[] _readerNames;

    private static EventWaitHandle _ewh;

    public PcScService(IOptions<ClientSettings> config, ILogger<PcScService> logger, IServiceProvider services)
    {
        _config = config.Value;
        _logger = logger;
        _services = services;
        _ewh = new EventWaitHandle(false, EventResetMode.AutoReset);
        _logger.LogDebug("PcScService is initiating");
    }

    private bool ExitRequested(ConsoleKeyInfo key)
    {
        return key.Modifiers == ConsoleModifiers.Control
               && key.Key == ConsoleKey.Q;
    }

    public void StartListening()
    {
        _logger.LogInformation("This program will monitor all SmartCard readers and display all status changes.");

        // Retrieve the names of all installed readers.
        _readerNames = GetReaderNames();

        if (NoReaderFound(_readerNames))
        {
            _logger.LogInformation("There are currently no readers installed.");

            return;
        }

        // Create smartcard monitor using a context factory. 
        // The context will be automatically released after monitor.Dispose()
        var monitorFactory = MonitorFactory.Instance;
        using (var monitor = monitorFactory.Create(SCardScope.System))
        {
            AttachToAllEvents(monitor); // Remember to detach, if you use this in production!

            ShowUserInfo(_readerNames);

            monitor.Start(_readerNames);

            _logger.LogInformation("Monitoring started.");

            _ewh.WaitOne();
            _logger.LogWarning("jumped over waitOne");
        }
    }

    public string GetCurrentCardId()
    {
        return _currentCardId;
    }

    private void ShowUserInfo(IEnumerable<string> readerNames)
    {
        foreach (var reader in readerNames)
        {
            _logger.LogInformation($"Start monitoring for reader {reader}.");
        }
    }

    private void AttachToAllEvents(ISCardMonitor monitor)
    {
        monitor.Initialized += (sender, args) =>
        {
            string uid;
            try
            {
                uid = ReadNfcCardUid();
                var signalrService = _services.GetRequiredService<IPaymentSignalrService>();
                signalrService.SendCardInformation(uid);
            }
            catch (NoSmartcardException)
            {
                uid = null;
            }

            DisplayEvent("Initialized", args, uid);
        };

        // Point the callback function(s) to the anonymous & static defined methods below.
        monitor.CardInserted += (sender, args) =>
        {
            var uid = ReadNfcCardUid();

            _currentCardId = uid;
            var signalrService = _services.GetRequiredService<IPaymentSignalrService>();
            signalrService.SendCardInformation(uid);

            DisplayEvent("CardInserted", args, uid);
        };

        monitor.CardRemoved += (sender, args) =>
        {
            var signalrService = _services.GetRequiredService<IPaymentSignalrService>();
            signalrService.SendRemovedCardInformation();
            _currentCardId = null;
            DisplayEvent("CardRemoved", args, null);
        };

        //monitor.StatusChanged += StatusChanged;
        monitor.MonitorException += MonitorException;
    }

    private void DisplayEvent(string eventName, CardStatusEventArgs args, string uid)
    {
        _logger.LogInformation(">> {0} Event for reader: {1} UID: {2} State: {3}",
            eventName,
            args.ReaderName,
            uid,
            args.State);
    }

    private void MonitorException(object sender, PCSCException ex)
    {
        _logger.LogInformation("Monitor exited due an error:");
        _logger.LogInformation(SCardHelper.StringifyError(ex.SCardError));
    }

    private string[] GetReaderNames()
    {
        using var context = _contextFactory.Establish(SCardScope.System);
        return context.GetReaders();
    }

    private bool NoReaderFound(ICollection<string> readerNames)
    {
        return readerNames == null || readerNames.Count < 1;
    }

    private string ReadNfcCardUid()
    {
        string uid;

        try
        {
            var contextFactory = ContextFactory.Instance;
            using var ctx = contextFactory.Establish(SCardScope.System);
            using var isoReader = new IsoReader(ctx, _readerNames[_config.CardReaderId], SCardShareMode.Shared,
                SCardProtocol.Any, false);
            var apdu = new CommandApdu(IsoCase.Case2Short, isoReader.ActiveProtocol)
            {
                CLA = 0xFF, // Class
                Instruction = InstructionCode.GetData,
                P1 = 0x00, // Parameter 1
                P2 = 0x00, // Parameter 2
                Le = 0x00 // Expected length of the returned data
            };

            var response = isoReader.Transmit(apdu);

            if (!response.HasData) return null;

            uid = BitConverter.ToString(response.GetData() ?? new byte[0]);
            _currentCardId = uid;
        }
        catch (Exception ex) when (ex is NoSmartcardException || ex is RemovedCardException)
        {
            uid = null;
        }

        return uid;
    }
}