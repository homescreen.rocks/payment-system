import {Component, Input, OnInit} from '@angular/core';
import {PointOfSaleInfo} from 'payment-lib';
import { faCreditCard } from '@fortawesome/free-regular-svg-icons';
import {faCalculator} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-point-of-sale-statusbar-item',
  templateUrl: './points-of-sale-statusbar-item.component.html',
  styleUrls: ['./points-of-sale-statusbar-item.component.scss']
})
export class PointsOfSaleStatusbarItemComponent implements OnInit {

  faCalculator = faCalculator;
  faCreditCard = faCreditCard;

  @Input() pos: PointOfSaleInfo;

  constructor() {
  }

  ngOnInit(): void {
  }

}
