import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PointsOfSaleStatusbarItemComponent } from './points-of-sale-statusbar-item.component';

describe('PointOfSaleStatusbarItemComponent', () => {
  let component: PointsOfSaleStatusbarItemComponent;
  let fixture: ComponentFixture<PointsOfSaleStatusbarItemComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PointsOfSaleStatusbarItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointsOfSaleStatusbarItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
