import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PointsOfSaleStatusbarComponent } from './points-of-sale-statusbar.component';

describe('PointsOfSaleStatusbarComponent', () => {
  let component: PointsOfSaleStatusbarComponent;
  let fixture: ComponentFixture<PointsOfSaleStatusbarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PointsOfSaleStatusbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointsOfSaleStatusbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
