import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {CreatePointOfSale, PointOfSaleInfo, PointsOfSaleService, UpdatePointOfSale} from 'payment-lib';
import {Observable, Subject} from 'rxjs';
import {BackendErrorHandler} from '../../helper/backend-error.handler';
import {faCircle} from '@fortawesome/free-solid-svg-icons';
import {ToastrService} from 'ngx-toastr';
import {catchError, tap} from 'rxjs/operators';

@Component({
  selector: 'app-points-of-sale-details',
  templateUrl: './points-of-sale-details.component.html',
  styleUrls: ['./points-of-sale-details.component.scss']
})
export class PointsOfSaleDetailsComponent implements OnInit, OnDestroy {

  oldPointOfSaleInfo: PointOfSaleInfo;

  private unsubscribe$ = new Subject<void>();

  pointOfSaleForm = this.fb.group({
    id: [null],
    name: ['', Validators.compose([
      Validators.required,
      Validators.minLength(3)
    ])],
  });
  faCircle = faCircle;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private pointsOfSaleService: PointsOfSaleService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    if (this.route.snapshot.data.pointOfSale === null) {
      this.oldPointOfSaleInfo = null;
      this.pointOfSaleForm.reset();
      return;
    } else {
      this.oldPointOfSaleInfo = this.route.snapshot.data.pointOfSale;
      this.pointOfSaleForm.patchValue(this.route.snapshot.data.pointOfSale);
    }
  }

  save(): void {
    const formValue = this.pointOfSaleForm.getRawValue() as PointOfSaleInfo;

    let response$: Observable<PointOfSaleInfo>;
    if (formValue.id === null) {
      const createMessage: CreatePointOfSale = {
        name: formValue.name
      };
      response$ = this.pointsOfSaleService.createPointOfSale(createMessage);
    } else {
      const updateMessage: UpdatePointOfSale = {
        name: formValue.name
      };
      response$ = this.pointsOfSaleService.updatePointOfSale(formValue.id, updateMessage);
    }
    response$.pipe(
      tap(() => {
        this.toastr.success('Point of Sale saved.', 'Success');
      }),
      catchError((err) => {
        this.toastr.error('Couldnt save Point of Sale!', 'Error');
        throw err;
      })
    ).subscribe(
      (pos) => {
        this.router.navigate(['/points-of-sale']);
      },
      BackendErrorHandler.pushBackendErrorsToForm(this.pointOfSaleForm)
    );
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
