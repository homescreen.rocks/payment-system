import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from 'payment-lib';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  public isAuthenticated$: Observable<boolean>;

  constructor(private authService: AuthService) {
  }

  ngOnInit() {
    this.isAuthenticated$ = this.authService.isLoggedIn$;
  }

}
