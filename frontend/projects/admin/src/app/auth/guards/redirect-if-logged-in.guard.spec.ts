import { TestBed } from '@angular/core/testing';

import { RedirectIfLoggedInGuard } from './redirect-if-logged-in.guard';

describe('RedirectIfLoggedInGuard', () => {
  let guard: RedirectIfLoggedInGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(RedirectIfLoggedInGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
