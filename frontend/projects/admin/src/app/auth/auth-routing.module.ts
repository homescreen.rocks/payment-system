import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AuthComponent} from './auth/auth.component';
import {LogoutComponent} from './logout/logout.component';
import {RedirectIfLoggedInGuard} from './guards/redirect-if-logged-in.guard';

export const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        component: LoginComponent,
      },
      {
        path: 'login',
        component: LoginComponent,
        canActivate: [RedirectIfLoggedInGuard]
      },
      {
        path: 'logout',
        component: LogoutComponent,
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {
}
