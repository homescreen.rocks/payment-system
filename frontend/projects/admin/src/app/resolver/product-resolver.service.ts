import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Product, ProductsService} from 'payment-lib';

@Injectable({
  providedIn: 'root'
})
export class ProductResolverService implements Resolve<Product> {
  constructor(private productApi: ProductsService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product> | Observable<never> {
    if (route.params.id === 'new') {
      return null;
    }
    return this.productApi.getProductById(route.params.id).pipe(
      catchError(e => {
        return of(null).pipe(
          tap(() => this.router.navigate(['404']))
        );
      })
    );
  }
}
