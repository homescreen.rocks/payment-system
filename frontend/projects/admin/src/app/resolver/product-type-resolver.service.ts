import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {ProductType, ProductTypesService} from 'payment-lib';

@Injectable({
  providedIn: 'root'
})
export class ProductTypeResolverService implements Resolve<ProductType> {
  constructor(private productTypeApi: ProductTypesService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ProductType> | Observable<never> {
    if (route.params.id === 'new') {
      return null;
    }
    return this.productTypeApi.getProductTypeById(route.params.id).pipe(
      catchError(e => {
        return of(null).pipe(
          tap(() => this.router.navigate(['404']))
        );
      })
    );
  }
}
