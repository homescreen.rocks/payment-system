import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AccountInfo, AccountsService} from 'payment-lib';

@Injectable({
  providedIn: 'root'
})
export class GetAccountResolverService implements Resolve<AccountInfo> {
  constructor(private accountsService: AccountsService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<AccountInfo | null> {
    const id = route.paramMap.get('id');
    if (id === 'new') {
      return null;
    }
    return this.accountsService.getAccountInfoById(id);
  }
}
