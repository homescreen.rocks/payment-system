import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {tap} from 'rxjs/operators';
import {AuthService} from 'payment-lib';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate() {
    return this.authService.isLoggedIn$.pipe(
      tap(isAuthenticated => {
        if (!isAuthenticated) {
          this.router.navigate(['auth', 'login']);
        }
      })
    );
  }
}
