import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {PointOfSaleInfo, PointsOfSaleService} from 'payment-lib';

@Injectable({
  providedIn: 'root'
})
export class PointsOfSaleSelectedGuard implements CanActivate {
  private currentPos: PointOfSaleInfo;

  constructor(private posService: PointsOfSaleService, private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    this.currentPos = JSON.parse(localStorage.getItem('currentPos'));
    if (this.currentPos != null) {
      this.posService.select(this.currentPos);
      return true;
    } else {
      this.router.navigate(['configuration']);
    }
  }
}
