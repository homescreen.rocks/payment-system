import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import {debounceTime, map, tap} from 'rxjs/operators';
import {merge, Observable} from 'rxjs';
import {FormControl} from '@angular/forms';
import {ConfirmDialogService} from '../../layout/confirm-dialog/confirm-dialog.service';
import {AccountFilter, AccountInfo, AccountsService, PagedDataSource, RelationalOperator, SortDirection} from 'payment-lib';
import {faEdit, faPlusSquare, faTrashAlt} from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss']
})
export class AccountsListComponent implements OnInit, AfterViewInit {
  faEdit = faEdit;
  faTrash = faTrashAlt;
  faPlus = faPlusSquare;

  columnsToDisplay = ['id', 'email', 'firstName', 'lastName', 'cardId', 'balance', 'overdraft', 'discount', 'locked', 'actions']; // TODO how to translate this?
  filterRow = ['id-filter', 'email-filter', 'firstName-filter', 'lastName-filter', 'cardId-filter', 'balance-filter', 'overdraft-filter', 'discount-filter',
    'locked-filter', 'actions-filter'];
  dataSource: PagedDataSource<AccountInfo, AccountFilter>;

  allFilters$: Observable<[keyof AccountFilter, RelationalOperator, string | number | boolean]>;

  emailFilterValue = new FormControl('');
  emailFilterValue$: Observable<['email', RelationalOperator, string]>;
  firstNameFilterValue = new FormControl('');
  firstNameFilterValue$: Observable<['firstName', RelationalOperator, string]>;
  lastNameFilterValue = new FormControl('');
  lastNameFilterValue$: Observable<['lastName', RelationalOperator, string]>;
  cardIdFilterValue = new FormControl('');
  cardIdFilterValue$: Observable<['cardId', RelationalOperator, string]>;
  balanceFilterValue = new FormControl('');
  balanceFilterValue$: Observable<['balance', RelationalOperator, string]>;
  overdraftFilterValue = new FormControl('');
  overdraftFilterValue$: Observable<['overdraft', RelationalOperator, string]>;
  discountFilterValue = new FormControl('');
  discountFilterValue$: Observable<['discount', RelationalOperator, string]>;
  lockedFilterValue = new FormControl('');
  lockedFilterValue$: Observable<['locked', RelationalOperator, boolean]>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private accountsService: AccountsService, private confirmDialog: ConfirmDialogService) {
  }

  ngOnInit() {
    this.dataSource = new PagedDataSource(this.accountsService.fetch, AccountFilter);
    this.dataSource.filter.setSort('email', SortDirection.ASC);
    this.dataSource.loadData();
  }

  ngAfterViewInit() {
    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    this.emailFilterValue$ = this.emailFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['email', RelationalOperator.CONTAINS_CI, v])
    );
    this.firstNameFilterValue$ = this.firstNameFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['firstName', RelationalOperator.CONTAINS_CI, v])
    );
    this.lastNameFilterValue$ = this.lastNameFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['lastName', RelationalOperator.CONTAINS_CI, v])
    );
    this.cardIdFilterValue$ = this.cardIdFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['cardId', RelationalOperator.CONTAINS_CI, v])
    );
    this.balanceFilterValue$ = this.balanceFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['balance', RelationalOperator.EQUALS, v])
    );
    this.overdraftFilterValue$ = this.overdraftFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['overdraft', RelationalOperator.EQUALS, v])
    );
    this.discountFilterValue$ = this.discountFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['discount', RelationalOperator.EQUALS, v])
    );

    this.lockedFilterValue$ = this.lockedFilterValue.valueChanges.pipe(
      debounceTime(300),
      map(v => ['locked', RelationalOperator.EQUALS, Boolean(v)])
    );

    this.allFilters$ = merge(
      this.emailFilterValue$,
      this.firstNameFilterValue$,
      this.lastNameFilterValue$,
      this.cardIdFilterValue$,
      this.balanceFilterValue$,
      this.overdraftFilterValue$,
      this.discountFilterValue$,
      this.lockedFilterValue$
    );

    merge(this.sort.sortChange, this.paginator.page, this.allFilters$)
      .pipe(
        tap((change: [keyof AccountFilter, RelationalOperator, string | boolean] | Sort | PageEvent) => {
            this.loadAccountInfosPage(change);
          }
        )
      )
      .subscribe();
  }

  loadAccountInfosPage<K extends keyof AccountFilter>(change: [K, RelationalOperator, string | boolean] | Sort | PageEvent) {
    if (change instanceof Array) {
      const key = change[0] as K;
      const operator = change[1];
      const value = change[2];
      if (value === '') {
        this.dataSource.filter.clearFilter(key);
      } else {
        // @ts-ignore
        this.dataSource.filter.setFilter(key, operator, value);
      }
    }
    this.dataSource.filter.clearSort();
    // @ts-ignore
    this.dataSource.filter.setSort(this.sort.active, this.sort._direction);
    this.dataSource.filter.setPage(this.paginator.pageIndex + 1);
    this.dataSource.filter.setPageSize(this.paginator.pageSize);
    this.dataSource.loadData();
  }

  delete(acc: AccountInfo) {
    this.confirmDialog.confirm(
      'Delete Account',
      'Do you really want to delete account ' + acc.email + ' (' + acc.id + ')?',
      'DELETE'
    ).then(() => {
      this.accountsService.deleteAccount(acc.id).subscribe();
    }, () => {
    });
  }
}
