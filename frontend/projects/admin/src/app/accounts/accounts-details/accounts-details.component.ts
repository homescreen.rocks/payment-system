import {Component, OnInit} from '@angular/core';
import {FormBuilder, ValidationErrors, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AccountInfo, AccountsService, Filter} from 'payment-lib';
import {Observable} from 'rxjs';
import {BackendErrorHandler} from '../../helper/backend-error.handler';
import {catchError, tap} from 'rxjs/operators';
import {ToastrService} from 'ngx-toastr';
import {StatusCodes} from 'http-status-codes';

@Component({
  selector: 'app-accounts-details',
  templateUrl: './accounts-details.component.html',
  styleUrls: ['./accounts-details.component.scss']
})
export class AccountsDetailsComponent implements OnInit {

  account: AccountInfo | null;
  validationErrors: ValidationErrors;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private accountService: AccountsService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.account = this.route.snapshot.data.account;
  }

  save(accountInfo: AccountInfo): void {
    this.accountService.save(accountInfo).pipe(
      tap(() => {
        this.toastr.success('Account saved.', 'Success');
      }),
      catchError((err) => {
        this.toastr.error('Couldnt save account!', 'Error');
        throw err;
      })
    ).subscribe(
      (acc) => {
        this.router.navigate(['/accounts']);
      },
      (err) => {
        if (err.status === StatusCodes.BAD_REQUEST) {
          console.log(err.error);
          this.validationErrors = err.error.errors;
        }
      },
    );
  }
}
