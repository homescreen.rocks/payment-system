import {Pipe, PipeTransform} from '@angular/core';
import {PurchaseTransactionEntry} from 'payment-lib';

@Pipe({
  name: 'concatProducts'
})
export class ConcatProductsPipe implements PipeTransform {

  transform(value: PurchaseTransactionEntry[], ...args: any[]): string {
    if (value === undefined || value === null || (Array.isArray(value) && value.length === 0)) {
      return '';
    }
    const x: Map<string, number> = new Map<string, number>();
    value.forEach(pt => {
      x.set(pt.product.name, x.get(pt.product.name) === undefined ? 1 : x.get(pt.product.name) + 1);
    });
    return [...x.entries()].map((tmp: [string, number]) => {
      return tmp[1] + 'x ' + tmp[0];
    }).join(', ');
  }

}
