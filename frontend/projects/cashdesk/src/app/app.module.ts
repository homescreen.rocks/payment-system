import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {DEFAULT_CURRENCY_CODE, NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatCommonModule, MatRippleModule} from '@angular/material/core';
import {MatSliderModule} from '@angular/material/slider';
import {MatTabsModule} from '@angular/material/tabs';
import {BrowserModule, HammerModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {far} from '@fortawesome/free-regular-svg-icons';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from '@danielmoncada/angular-datetime-picker';
import {NgbModalModule, NgbToastModule} from '@ng-bootstrap/ng-bootstrap';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {CartEntryComponent} from './components/cart-entry/cart-entry.component';
import {CartComponent} from './components/cart/cart.component';
import {ChargeButtonComponent} from './components/charge-button/charge-button.component';
import {ChargeSelectorComponent} from './components/charge-selector/charge-selector.component';
import {ChargeSidebarComponent} from './components/charge-sidebar/charge-sidebar.component';
import {ChargeValueComponent} from './components/charge-value/charge-value.component';
import {CheckoutButtonComponent} from './components/checkout-button/checkout-button.component';
import {CheckoutSidebarComponent} from './components/checkout-sidebar/checkout-sidebar.component';
import {CustomerDetailsComponent} from './components/customer-details/customer-details.component';
import {FloatingLinkButtonComponent} from './components/floating-link-button/floating-link-button.component';
import {ProductSelectorComponent} from './components/product-selector/product-selector.component';
import {ProductComponent} from './components/product/product.component';
import {OrderIsCancelablePipe} from './directives/order-is-cancelable.pipe';
import {UseWindowHeightDirective} from './directives/use-window-height.directive';
import {CashdeskscreenComponent} from './structure/cashdeskscreen/cashdeskscreen.component';
import {ChargescreenComponent} from './structure/chargescreen/chargescreen.component';
import {ConfigurationComponent} from './structure/configuration/configuration.component';
import {StartscreenComponent} from './structure/startscreen/startscreen.component';
import {TransactionscreenComponent} from './structure/transactionscreen/transactionscreen.component';
import {AuthService, LibModule} from 'payment-lib';
import {LongPressDirective} from './directives/long-press.directive';
import {ProductTypeCardComponent} from './components/product-type-card/product-type-card.component';
import {filter, tap} from 'rxjs/operators';
import {ToastrModule} from 'ngx-toastr';
import {environment} from '../environments/environment';
import {ApplicationOfflineScreenComponent} from './structure/application-offline-screen/application-offline-screen.component';
import {OnlineStatusService} from '../../../payment-lib/src/lib/services/online-status/online-status.service';
import {Router} from '@angular/router';
import {LockButtonComponent} from './components/lock-button/lock-button.component';
import {OAuthModule} from 'angular-oauth2-oidc';

@NgModule({
  declarations: [
    AppComponent,
    CashdeskscreenComponent,
    ProductSelectorComponent,
    CheckoutSidebarComponent,
    CustomerDetailsComponent,
    CartComponent,
    ProductComponent,
    CartEntryComponent,
    CheckoutButtonComponent,
    UseWindowHeightDirective,
    ConfigurationComponent,
    ChargescreenComponent,
    ChargeSelectorComponent,
    StartscreenComponent,
    FloatingLinkButtonComponent,
    ChargeSidebarComponent,
    ChargeValueComponent,
    ChargeButtonComponent,
    OrderIsCancelablePipe,
    TransactionscreenComponent,
    LongPressDirective,
    ProductTypeCardComponent,
    ApplicationOfflineScreenComponent,
    LockButtonComponent
  ],
  imports: [
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatCommonModule,
    MatSliderModule,
    MatTabsModule,
    MatRippleModule,
    NgbModalModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: ['/api/'],
        sendAccessToken: true
      }
    }),
    FontAwesomeModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    NgbToastModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-bottom-center'
    }),
    LibModule,
    HammerModule
  ],
  providers: [
    // todo: setting DEFAULT_CURRENCY_CODE is deprecated and will be removed in angular 10
    // we can remove this line in angular 10 as the currency will be taken from the current locale then
    // https://angular.io/api/core/DEFAULT_CURRENCY_CODE
    {provide: DEFAULT_CURRENCY_CODE, useValue: environment.defaultCurrency},
    AuthService,
    OnlineStatusService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    library: FaIconLibrary,
    private onlineStatusService: OnlineStatusService,
    private router: Router
  ) {
    library.addIconPacks(fas);
    library.addIconPacks(far);

    this.onlineStatusService.browserIsOnline$.pipe(
      filter(onlineStatus => onlineStatus === false),
      tap(() => this.router.navigate(['offline'])),
    ).subscribe();
  }
}
