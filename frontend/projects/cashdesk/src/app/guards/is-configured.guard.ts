import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, switchMap, take} from 'rxjs';
import {ConfigurationService} from '../services/configuration.service';
import {AuthService, PointsOfSaleService} from 'payment-lib';
import {filter, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class IsConfiguredGuard implements CanActivate {

  constructor(
    private lockscreenService: ConfigurationService,
    private router: Router,
    private authService: AuthService,
    private posService: PointsOfSaleService
  ) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.lockscreenService.pointOfSaleId === null) {
      this.router.navigate(['/configuration']);
      return false;
    } else {
      this.authService.isLoggedIn$.pipe(
        take(1),
        switchMap(() => this.posService.getPointOfSaleById(this.lockscreenService.pointOfSaleId)),
        tap(pos => this.posService.select(pos)),
      ).subscribe();
    }
    return true;
  }
}
