import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CashdeskscreenComponent} from './structure/cashdeskscreen/cashdeskscreen.component';
import {ConfigurationComponent} from './structure/configuration/configuration.component';
import {IsConfiguredGuard} from './guards/is-configured.guard';
import {IsUnlockedGuard} from './guards/is-unlocked.guard';
import {LogoutGard} from './guards/logout-gard.service';
import {ChargescreenComponent} from './structure/chargescreen/chargescreen.component';
import {StartscreenComponent} from './structure/startscreen/startscreen.component';
import {TransactionscreenComponent} from './structure/transactionscreen/transactionscreen.component';
import {ApplicationOfflineScreenComponent} from './structure/application-offline-screen/application-offline-screen.component';

const routes: Routes = [
  {path: 'configuration', component: ConfigurationComponent},
  {path: 'offline', component: ApplicationOfflineScreenComponent, canActivate: [LogoutGard]},
  {path: 'start', component: StartscreenComponent, canActivate: [IsUnlockedGuard, IsConfiguredGuard]},
  {path: 'cashdesk', component: CashdeskscreenComponent, canActivate: [IsUnlockedGuard, IsConfiguredGuard]},
  {path: 'charge', component: ChargescreenComponent, canActivate: [IsUnlockedGuard, IsConfiguredGuard]},
  {path: 'transaction', component: TransactionscreenComponent, canActivate: [IsUnlockedGuard, IsConfiguredGuard]},
  {
    path: '',
    redirectTo: '/cashdesk',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
