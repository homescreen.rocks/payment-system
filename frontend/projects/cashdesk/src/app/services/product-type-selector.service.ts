import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {Filter, ProductType, ProductTypeFilter, ProductTypesService} from 'payment-lib';

@Injectable({
  providedIn: 'root'
})
export class ProductTypeSelectorService {
  private productTypes: ProductType[];
  private currentProductType = new Subject<ProductType>();
  public currentProductType$ = this.currentProductType.asObservable();

  constructor(private productTypesService: ProductTypesService) {
    this.productTypesService.getAllProductTypesFiltered(new Filter(ProductTypeFilter)).subscribe(pts => {
      this.productTypes = pts;
      if (this.productTypes.length > 0) {
        this.jump(this.productTypes[0]);
      }
    });
  }

  jump(productType: ProductType) {
    this.currentProductType.next(productType);
  }
}
