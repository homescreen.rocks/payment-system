import {Injectable} from '@angular/core';
import {AuthService} from 'payment-lib';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {CardInfo, CreateChargeTransaction, SignalrClientService, TransactionsService} from 'payment-lib';
import {ConfigurationService} from '../configuration.service';

@Injectable({
  providedIn: 'root'
})
export class ChargeService {

  private chargeValue = 0;
  private chargeSubject: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  private currentCard$: Observable<CardInfo | null> = this.eventSubscriberService.currentCard;
  private currentCard: CardInfo;

  constructor(private configService: ConfigurationService,
              private transactionsService: TransactionsService,
              private eventSubscriberService: SignalrClientService) {
    this.currentCard$.subscribe((c: CardInfo | null) => {
      if (c === null) {
        this.chargeValue = 0;
        this.chargeSubject.next(this.chargeValue);
      }
      this.currentCard = c;
    });
  }

  get currentChargeAmount(): Observable<number> {
    return this.chargeSubject.asObservable();
  }

  public changeChargeAmount(amount: number) {
    // Charge Account
    if (amount > 0) {
      if (this.chargeValue < 0) {
        this.chargeValue = 0;
      }
      this.chargeValue += amount;
      this.chargeSubject.next(this.chargeValue);
    }
    // Uncharge Account
    if (amount < 0) {
      this.chargeValue = amount;
      this.chargeSubject.next(this.chargeValue);
    }
  }

  public checkout(comment: string): Observable<boolean> {
    if (!this.currentCard || this.currentCard.account.locked) {
      return of(false);
    }

    const createChargeTransaction: CreateChargeTransaction = {
      amount: this.chargeValue,
      accountId: this.currentCard.account.id,
      pointOfSaleId: this.configService.pointOfSaleId,
      comment
    };

    const response = this.transactionsService.chargeAccount(createChargeTransaction);
    return response.pipe(
      tap(ci => {
        this.currentCard = ci;
        this.chargeValue = 0;
        this.chargeSubject.next(this.chargeValue);
      }),
      catchError((err, caught) => {
        console.log(err);
        console.log(caught);
        return of(null);
      }),
      map(ci => {
        return !!ci;
      })
    );
  }
}
