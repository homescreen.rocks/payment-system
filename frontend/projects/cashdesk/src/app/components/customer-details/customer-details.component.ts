import {Component, OnInit, TemplateRef} from '@angular/core';
import {Observable} from 'rxjs';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {
  CardInfo,
  Filter,
  PagedResult,
  RelationalOperator,
  SignalrClientService,
  SortDirection,
  TransactionResponse,
  TransactionsFilter,
  TransactionsService
} from 'payment-lib';

@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.scss']
})
export class CustomerDetailsComponent implements OnInit {
  public currentCard: CardInfo | null;
  public currentDiscount$: Observable<number>;

  // public currentCard$: Observable<CardInfo>;
  public modalInstance: NgbModalRef;
  public currentTransactions: PagedResult<TransactionResponse>;

  constructor(private eventSubscriber: SignalrClientService, private modal: NgbModal, private transactionsService: TransactionsService) {
  }

  ngOnInit() {
    this.eventSubscriber.currentCard.subscribe((c) => {
      this.currentCard = c;
      if (this.modalInstance && this.currentCard === null) {
        this.modalInstance.close();
      }
    });
    this.currentDiscount$ = this.eventSubscriber.currentDiscount;
  }

  public async showAccountTransactions(modalRef: TemplateRef<any>) {
    if (!this.currentCard) {
      return;
    }
    this.modalInstance = this.modal.open(modalRef, {
      size: 'lg'
    });

    this.loadTransactionHistory();

    this.modalInstance.result.then(() => {
      this.currentTransactions = null;
    }, () => {
      this.currentTransactions = null;
    });
  }

  cancelTransaction(trans: TransactionResponse) {
    this.transactionsService.cancelTransaction(trans).subscribe(_ => {
      this.loadTransactionHistory();
    });
  }

  isCardValid(): string {
    if (this.currentCard === null) {
      return 'noCard';
    }
    if (this.currentCard.account === null) {
      return 'noAccount';
    }
    if (this.currentCard.account.locked) {
      return 'accountLocked';
    }
    return 'valid';
  }

  private loadTransactionHistory() {
    const filter = new Filter(TransactionsFilter, 1, 50);
    filter.setFilter('Account.Id', RelationalOperator.EQUALS, this.currentCard.account.id);
    filter.setSort('timestamp', SortDirection.DESC);
    filter.setFilter('isCancelled', RelationalOperator.EQUALS, false);
    this.transactionsService.getFilteredTransactions(filter).subscribe(allTransactions => {
        this.currentTransactions = allTransactions;
        this.currentTransactions.results.forEach(transaction => {
          transaction.entriesGrouped = [];
          if (transaction.transactionType === 'PurchaseTransaction') {
            transaction.entries.forEach(item => {
              const sameItem = transaction.entriesGrouped.find(element => {
                return element.product.id === item.product.id && element.price === item.price;
              });
              if (sameItem === undefined) {
                transaction.entriesGrouped.push({product: item.product, price: item.price, amount: 1});
              } else {
                sameItem.amount++;
              }
            });
          }
          transaction.entriesGrouped.sort((a, b) => {
            if (a.product.name === b.product.name) {
              return (b.price - a.price);
            } else if (a.product.name > b.product.name) {
              return 1;
            } else if (a.product.name < b.product.name) {
              return -1;
            }
          });
        });
        console.log(this.currentTransactions.results);
      }
    );
  }
}
