import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';
import {CardInfo, Product, SignalrClientService} from 'payment-lib';
import {CartService} from '../../services/cart-service/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  // private currentItemsSubject
  public currentItems$ = this.cartService.currentCart.pipe(
    map(cart => cart.sort()) // TODO sort correctly
  );

  public itemsInCart$: Observable<number> = this.currentItems$.pipe(
    map(cart => cart.length)
  );

  public currentCard$: Observable<CardInfo | null> = this.eventSubscriberService.currentCard;
  public lastChangedItem$: Observable<Product> = this.cartService.lastChangedItem.asObservable();
  public currentSum$: Observable<number> = this.cartService.totalCheckoutSum;

  constructor(private cartService: CartService, private eventSubscriberService: SignalrClientService, public router: Router) {
  }

  ngOnInit() {
  }

  onProductRemoved(index: number) {
    this.cartService.removeFromCart(index);
  }
}
