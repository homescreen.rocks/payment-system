import {Component, OnInit, TemplateRef} from '@angular/core';
import {CardInfo, Product, SignalrClientService} from 'payment-lib';
import {combineLatest} from 'rxjs';
import {map} from 'rxjs/operators';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {CartService} from '../../services/cart-service/cart.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-checkout-button',
  templateUrl: './checkout-button.component.html',
  styleUrls: ['./checkout-button.component.scss']
})

export class CheckoutButtonComponent implements OnInit {
  public currentCart$ = this.cartService.currentCart;
  public currentCard$ = this.eventSubscriber.currentCard;
  public totalCheckoutSum$ = this.cartService.totalCheckoutSum;

  public modalInstance: NgbModalRef;
  comments: string;

  public isValidCheckout$ = combineLatest([this.currentCart$, this.totalCheckoutSum$, this.currentCard$]).pipe(
    map((c: [Product[], number, CardInfo]) => {
      if (c[2] === null) {
        return false;
      }
      if (c[2].account === null) {
        return false;
      }

      if ((c[2].account.balance + c[2].account.overdraft) < c[1]) {
        return false;
      }

      return c[0].length !== 0;
    })
  );

  constructor(
    private cartService: CartService,
    private eventSubscriber: SignalrClientService,
    private modal: NgbModal,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
  }

  checkout(comment: string) {
    this.comments = null;
    this.cartService.checkout(comment).subscribe(res => {
      this.toastr.success('Checkout successful.', 'Success');
    }, err => {
      this.toastr.error('Error while processing checkout! The transaction has been canceled!', 'Error', {disableTimeOut: true});
    });
  }

  public async showCheckoutConfirmationModal(modalRef: TemplateRef<any>) {
    this.modalInstance = this.modal.open(modalRef, {
      size: 'lg'
    });

    this.modalInstance.result.then((comments: string) => {
      this.checkout(comments);
    }, () => {
      this.toastr.warning('Checkout canceled!', 'Cancelled');
    });
  }
}
