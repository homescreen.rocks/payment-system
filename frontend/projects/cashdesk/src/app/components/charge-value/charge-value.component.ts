import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {ChargeService} from '../../services/charge-service/charge.service';

@Component({
  selector: 'app-charge-value',
  templateUrl: './charge-value.component.html',
  styleUrls: ['./charge-value.component.scss']
})
export class ChargeValueComponent implements OnInit {
  public currentChargeValue$: Observable<number> = this.chargeService.currentChargeAmount;

  constructor(private chargeService: ChargeService) {
  }

  ngOnInit() {
  }
}
