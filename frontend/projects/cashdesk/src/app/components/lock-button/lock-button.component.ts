import { Component, OnInit } from '@angular/core';
import {AuthService} from 'payment-lib';

@Component({
  selector: 'app-lock-button',
  templateUrl: './lock-button.component.html',
  styleUrls: ['./lock-button.component.scss']
})
export class LockButtonComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

}
