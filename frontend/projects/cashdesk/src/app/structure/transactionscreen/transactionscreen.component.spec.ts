import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TransactionscreenComponent } from './transactionscreen.component';

describe('TransactionscreenComponent', () => {
  let component: TransactionscreenComponent;
  let fixture: ComponentFixture<TransactionscreenComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionscreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionscreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
