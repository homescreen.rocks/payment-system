import {Component, OnInit} from '@angular/core';
import {
  Filter,
  ProductType,
  ProductTypeFilter,
  ProductTypesService,
  RelationalOperator,
  TransactionResponse,
  TransactionsFilter,
  TransactionsService
} from 'payment-lib';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-transactionscreen',
  templateUrl: './transactionscreen.component.html',
  styleUrls: ['./transactionscreen.component.scss']
})
export class TransactionscreenComponent implements OnInit {
  productTypes: ProductType[];
  transactions: TransactionResponse[];

  purchasedProducts = [];
  purchasedTargetProducts = [];

  targetProductType: ProductType = null;

  dateTimeValue: any;

  private dateTimeFrom: Date = null;
  private dateTimeTill: Date = null;

  constructor(private productTypesService: ProductTypesService, private transactionsService: TransactionsService) {
    const filter = new Filter(TransactionsFilter, 1, 50);
    this.collectData(filter);
  }

  ngOnInit() {
  }

  public setTargetProductType(tpt: ProductType) {
    if (this.targetProductType != null && this.targetProductType.id === tpt.id) {
      this.targetProductType = null;
    } else {
      this.targetProductType = tpt;
      this.groupTransactions(this.transactions);
    }
  }

  public groupTransactions(transactions: TransactionResponse[]): void {

    this.purchasedProducts = [];
    this.purchasedTargetProducts = [];

    this.transactions
      .filter(t => t.transactionType === 'PurchaseTransaction')
      .map(t => t.entriesGrouped.forEach(entrie => this.purchasedProducts.push({
        user: t.account.email,
        time: t.timestamp,
        product: entrie.product.name,
        productTypeId: entrie.product.productType.id
      })));

    this.purchasedTargetProducts = this.purchasedProducts
      .filter(e => e.productTypeId === this.targetProductType.id)
      .reduce((r, pp) => {
        r[pp.product] = r[pp.product] || [];
        r[pp.product].push({name: pp.user, time: pp.time});
        return r;
      }, Object.create(null));
  }

  public onChange(data) {
    this.dateTimeFrom = data[0];
    this.dateTimeTill = data[1];

    const filter = new Filter(TransactionsFilter, 1, 50);
    filter.setFilter('timestamp', RelationalOperator.GREATER_THAN_OR_EQUAL_TO, this.dateTimeFrom);
    filter.setFilter('timestamp', RelationalOperator.LESS_THAN_OR_EQUAL_TO, this.dateTimeTill);
    this.collectData(filter);
  }

  public resetDateTimePicker() {
    this.dateTimeValue = null;
    this.dateTimeFrom = null;
    this.dateTimeTill = null;
    const filter = new Filter(TransactionsFilter, 1, 50);
    this.collectData(filter);
  }

  count(value: any): number {
    let i = 0;
    // tslint:disable-next-line:forin
    for (const v in value) {
      i++;
    }
    return i;
  }

  private collectData(filter: Filter<TransactionsFilter>) {
    this.getProductTypes();
    this.getTransactions(filter);
  }

  private getProductTypes() {
    this.productTypesService.getAllProductTypesFiltered(new Filter(ProductTypeFilter)).subscribe(pt => {
      this.productTypes = pt;
    });
  }

  private getTransactions(filter) {
    this.transactionsService.getAllTransactionsFiltered(filter).subscribe(allTransactions => {
      this.transactions = allTransactions;
      this.transactions.forEach(transaction => {
        transaction.entriesGrouped = [];
        if (transaction.transactionType === 'PurchaseTransaction') {
          transaction.entries.forEach(item => {
            if (!transaction.entriesGrouped.find(element => {
              return element.product.id === item.product.id && element.price === item.price;
            })) {
              transaction.entriesGrouped.push({product: item.product, price: item.price, amount: 1});
            } else {
              transaction.entriesGrouped.find(itemInGroup => {
                if (itemInGroup.product.id === item.product.id && itemInGroup.price === item.product.price) {
                  itemInGroup.amount++;
                }
                return itemInGroup.product === item.product;
              });
            }
          });
        }
        transaction.entriesGrouped.sort((a, b) => {
          if (a.product.name === b.product.name) {
            return (b.price - a.price);
          } else if (a.product.name > b.product.name) {
            return 1;
          } else if (a.product.name < b.product.name) {
            return -1;
          }
        });
      });
    });
  }
}
