export const environment = {
  production: true,
  defaultCurrency: 'EUR', // The ISO 4217 currency code, such as USD for the US dollar and EUR for the euro.
};
