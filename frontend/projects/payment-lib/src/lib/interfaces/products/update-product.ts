import {uuid} from '../../types/uuid';

export interface UpdateProduct {
  name: string;
  price: number;
  description: string;
  image: string;
  order: number;
  locked: boolean;
  productTypeId: uuid;
}
