import {uuid} from '../../types/uuid';

export interface AccountInfo {
  id: uuid;
  email: string;
  firstName: string;
  lastName: string;
  cardId: string | null;
  balance: number;
  overdraft: number;
  discount: number;
  locked: boolean;
}
