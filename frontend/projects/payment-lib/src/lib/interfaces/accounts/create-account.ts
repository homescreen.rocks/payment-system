export interface CreateAccount {
  email: string;
  firstName: string;
  lastName: string;
  cardId: string;
  overdraft: number;
  discount: number;
}
