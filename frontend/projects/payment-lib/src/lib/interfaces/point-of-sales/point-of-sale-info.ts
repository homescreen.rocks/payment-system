import {uuid} from '../../types/uuid';

export interface PointOfSaleInfo {
  id: uuid;
  name: string;
  hasNfcClient: boolean;
  hasCashDeskClient: boolean;
  isReady: boolean;
}
