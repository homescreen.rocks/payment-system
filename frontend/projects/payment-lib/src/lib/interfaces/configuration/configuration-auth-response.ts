export interface ConfigurationAuthResponse {
  idpUrl: string;
  clientId: string;
}
