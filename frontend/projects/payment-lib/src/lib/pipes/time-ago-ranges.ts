export enum TimeAgoRanges {
  seconds,
  minutes,
  hours,
  days,
  longer
}
