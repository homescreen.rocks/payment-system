import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, EMPTY, Observable} from 'rxjs';
import {concatMap, expand, map, take, toArray} from 'rxjs/operators';
import {PointOfSaleInfo} from '../../interfaces/point-of-sales/point-of-sale-info';
import {CreatePointOfSale} from '../../interfaces/point-of-sales/create-point-of-sale';
import {UpdatePointOfSale} from '../../interfaces/point-of-sales/update-point-of-sale';
import {PagedResult} from '../paged-result';
import {AbstractApiService} from '../abstract-api.service';
import {Filter} from '../../filter/filter';
import {RelationalOperator} from '../../filter/relational-operator';
import {PointOfSaleFilter} from './point-of-sale-filter';
import {uuid} from '../../types/uuid';

@Injectable({
  providedIn: 'root'
})
export class PointsOfSaleService extends AbstractApiService<PointOfSaleInfo> {
  baseUrl = '/api/v1/PointsOfSale';
  private currentPos = new BehaviorSubject<PointOfSaleInfo | null>(null);
  currentPos$ = this.currentPos.asObservable();

  constructor(public http: HttpClient) {
    super();
  }

  getFilteredPointsOfSale(filter: Filter<PointOfSaleFilter>): Observable<PagedResult<PointOfSaleInfo>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  public fetch = (filter: Filter<PointOfSaleFilter>) => this.getFilteredPointsOfSale(filter);

  getPointOfSaleById(id: uuid): Observable<PointOfSaleInfo> {
    const filter = new Filter(PointOfSaleFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throw new Error('PointOfSale with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift())
    );
  }

  createPointOfSale(createMessage: CreatePointOfSale) {
    return this.postGeneric<CreatePointOfSale, PointOfSaleInfo>(this.baseUrl, createMessage);
  }

  updatePointOfSale(posId: uuid, updateMessage: UpdatePointOfSale) {
    return this.putGeneric<UpdatePointOfSale, PointOfSaleInfo>(this.baseUrl + '/' + posId, updateMessage);
  }

  select(newPos: PointOfSaleInfo) {
    this.currentPos$.pipe(
      take(1)
    ).subscribe(currentPos => {
      if (currentPos === null && newPos !== null || currentPos !== null && newPos === null || newPos.id !== currentPos.id) {
        this.currentPos.next(newPos);
      }
      return;
    });
  }

  getAllPointsOfSaleFiltered(filter: Filter<PointOfSaleFilter>): Observable<PointOfSaleInfo[]> {
    filter.setPage(1);
    return this.getFilteredPointsOfSale(filter).pipe(
      expand(page => {
        if (page.currentPage < page.pageCount) {
          filter.setPage(page.currentPage + 1);
          return this.getFilteredPointsOfSale(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray()
    );
  }
}
