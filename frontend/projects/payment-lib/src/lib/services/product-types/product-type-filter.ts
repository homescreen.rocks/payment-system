import {AbstractFilter} from '../../filter/abstract-filter';
import {RelationalOperator} from '../../filter/relational-operator';

export class ProductTypeFilter extends AbstractFilter {
  public id: Map<RelationalOperator, string | string[]> = new Map<RelationalOperator, string | string[]>();
  public name: Map<RelationalOperator, string> = new Map<RelationalOperator, string>();
  public order: Map<RelationalOperator, number> = new Map<RelationalOperator, number>();
  public locked: Map<RelationalOperator, boolean> = new Map<RelationalOperator, boolean>();
}
