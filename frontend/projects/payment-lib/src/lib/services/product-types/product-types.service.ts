import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {concatMap, expand, map, toArray} from 'rxjs/operators';
import {ProductTypeFilter} from './product-type-filter';
import {ProductType} from '../../interfaces/product-types/product-type';
import {PagedResult} from '../paged-result';
import {CreateProductType} from '../../interfaces/product-types/create-product-type';
import {AbstractApiService} from '../abstract-api.service';
import {Filter} from '../../filter/filter';
import {RelationalOperator} from '../../filter/relational-operator';
import {uuid} from '../../types/uuid';
import {UpdateProductType} from '../../interfaces/product-types/update-product-type';
import {SortDirection} from '../../filter/sort-direction';

@Injectable({
  providedIn: 'root'
})
export class ProductTypesService extends AbstractApiService<ProductType> {
  baseUrl = '/api/v1/ProductTypes';

  constructor(public http: HttpClient) {
    super();
  }

  getFilteredProductTypes(filter: Filter<ProductTypeFilter>): Observable<PagedResult<ProductType>> {
    return this.getFiltered(this.baseUrl, filter);
  }

  public fetch = (filter: Filter<ProductTypeFilter>) => this.getFilteredProductTypes(filter);

  getProductTypeById(id: uuid): Observable<ProductType | null> {
    const filter = new Filter(ProductTypeFilter, 1, 1);
    filter.setFilter('id', RelationalOperator.EQUALS, id);
    return this.getFiltered(this.baseUrl, filter).pipe(
      map(r => {
        if (r.rowCount !== 1) {
          throw new Error('ProductType with id ' + id + ' not found');
        } else {
          return r;
        }
      }),
      map(pagedResult => pagedResult.results.shift())
    );
  }

  createProductType(createMessage: CreateProductType): Observable<ProductType> {
    return this.postGeneric<CreateProductType, ProductType>(this.baseUrl, createMessage);
  }

  updateProductType(id: uuid, updateMessage: UpdateProductType): Observable<ProductType> {
    return this.putGeneric<UpdateProductType, ProductType>(this.baseUrl + '/' + id, updateMessage);
  }

  delete(productType: ProductType) {
    return this.deleteGeneric<null>(this.baseUrl + '/' + productType.id);
    // return this.http.delete(this.baseUrl + '/' + productType.id);
  }

  getAllProductTypesFiltered(filter: Filter<ProductTypeFilter>): Observable<ProductType[]> {
    filter.setPage(1);
    if (filter.getSort('order') === SortDirection.NONE) {
      filter.setSort('order', SortDirection.ASC);
    }
    return this.getFilteredProductTypes(filter).pipe(
      expand(page => {
        if (page.currentPage < page.pageCount) {
          filter.setPage(page.currentPage + 1);
          return this.getFilteredProductTypes(filter);
        } else {
          return EMPTY;
        }
      }),
      concatMap(responses => responses.results),
      toArray(),
    );
  }
}
